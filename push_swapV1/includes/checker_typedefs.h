/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_typedefs.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/15 13:42:25 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/25 07:46:44 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_TYPEDEFS_H
# define CHECKER_TYPEDEFS_H

typedef struct	s_memory
{
	long			*stack_a;
	long			*stack_b;
	int				size_a;
	int				size_b;
	short			opts;
	unsigned int	nb_inst;
}				t_memory;

typedef enum	e_opts
{
	CK_OPTS_V,
	CK_OPTS_S,
	CK_OPTS_O
}				t_opts;

#endif
