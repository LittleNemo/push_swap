/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/15 13:28:19 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/25 07:36:51 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_H
# define CHECKER_H

# include <limits.h>
# include <stdlib.h>
# include <stdio.h>
# include "../srcs/ft_printf/includes/ft_printf.h"
# include "checker_defines.h"
# include "checker_typedefs.h"

int				ck_error(int error, int c);

void			ck_get_opts(t_memory *memory, int *ac, char **av);
void			ck_get_values(t_memory *memory, int size, char **values);

void			ck_reading(t_memory *memory);
void			ck_op_push_a(t_memory *memory);
void			ck_op_push_b(t_memory *memory);
void			ck_op_rotate_a(t_memory *memory);
void			ck_op_rotate_b(t_memory *memory);
void			ck_op_rotate_r(t_memory *memory);
void			ck_op_rrotate_a(t_memory *memory);
void			ck_op_rrotate_b(t_memory *memory);
void			ck_op_rrotate_r(t_memory *memory);
void			ck_op_swap_a(t_memory *memory);
void			ck_op_swap_b(t_memory *memory);
void			ck_op_swap_s(t_memory *memory);

int				ck_issorted(long *stack, int size);

void			ck_print_stack(t_memory *memory, char *inst);

#endif
