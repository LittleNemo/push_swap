/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_defines.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/11 11:42:58 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/23 10:46:01 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_DEFINES_H
# define PUSH_SWAP_DEFINES_H

# ifndef PS_USAGE
#  define PS_USAGE			"usage: ./push_swap [-v[s]o] value1 ..."
# endif
# ifndef PS_USAGE_ALT
#  define PS_USAGE_ALT		"       ./push_swap [-v[s]o] \"value1 ...\""
# endif
# ifndef PS_ERROR_ARGS
#  define PS_ERROR_ARGS		"push_swap: illegal option -- "
# endif
# ifndef PS_ERROR_DOUBLE
#  define PS_ERROR_DOUBLE	"Error (push_swap): a value appears twice"
# endif
# ifndef PS_ERROR_MALLOC
#  define PS_ERROR_MALLOC	"Error (push_swap): malloc failed"
# endif
# ifndef PS_ERROR_NOT_INT
#  define PS_ERROR_NOT_INT	"Error (push_swap): a value isn't an int"
# endif
# ifndef PS_ERROR_NOT_NUM
#  define PS_ERROR_NOT_NUM	"Error (push_swap): a value isn't numerical"
# endif
# ifndef PS_ERROR_UNKNOW
#  define PS_ERROR_UNKNOW	"Error (push_swap): unknow"
# endif

#endif
