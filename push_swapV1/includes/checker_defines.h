/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_defines.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/15 13:37:02 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/16 11:46:56 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_DEFINES_H
# define CHECKER_DEFINES_H

# ifndef CK_USAGE
#  define CK_USAGE			"usage: ./checker [-v[s]o] value1 ..."
# endif
# ifndef CK_USAGE_ALT
#  define CK_USAGE_ALT		"       ./checker [-v[s]o] \"value1 ...\""
# endif
# ifndef CK_ERROR_ARGS
#  define CK_ERROR_ARGS		"checker: illegal option -- "
# endif
# ifndef CK_ERROR_DOUBLE
#  define CK_ERROR_DOUBLE	"Error (checker): a value appears twice"
# endif
# ifndef CK_ERROR_FORMAT
#  define CK_ERROR_FORMAT	"Error (checker): instruction not well formated"
# endif
# ifndef CK_ERROR_MALLOC
#  define CK_ERROR_MALLOC	"Error (checker): malloc failed"
# endif
# ifndef CK_ERROR_NOT_INT
#  define CK_ERROR_NOT_INT	"Error (checker): a value isn't an int"
# endif
# ifndef CK_ERROR_NOT_NUM
#  define CK_ERROR_NOT_NUM	"Error (checker): a value isn't numerical"
# endif
# ifndef CK_ERROR_UNKNOW
#  define CK_ERROR_UNKNOW	"Error (checker): unknow"
# endif

#endif
