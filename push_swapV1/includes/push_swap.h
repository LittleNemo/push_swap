/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/02 13:07:22 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/12 18:03:17 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <limits.h>
# include <stdlib.h>
# include <stdio.h>
# include "../srcs/ft_printf/includes/ft_printf.h"
# include "push_swap_defines.h"
# include "push_swap_typedefs.h"

int				ps_error(int error, int c);

void			ps_get_opts(t_memory *memory, int *ac, char **av);
void			ps_get_values(t_memory *memory, int size, char **values);

void			ps_sort(t_memory *memory);
void			ps_op_push_a(t_memory *memory);
void			ps_op_push_b(t_memory *memory);
void			ps_op_rotate_a(t_memory *memory);
void			ps_op_rotate_b(t_memory *memory);
void			ps_op_rotate_r(t_memory *memory);
void			ps_op_rrotate_a(t_memory *memory);
void			ps_op_rrotate_b(t_memory *memory);
void			ps_op_rrotate_r(t_memory *memory);
void			ps_op_swap_a(t_memory *memory);
void			ps_op_swap_b(t_memory *memory);
void			ps_op_swap_s(t_memory *memory);

double			ps_find_median(t_stack *stack);
int				ps_find_i_median(t_stack stack, long median);

int				ps_cnt_sorted(long *stack, int size);
int				ps_issorted(t_stack stack);
int				ps_isrsorted(long *stack, int size);
int				ps_find_max_index(t_stack stack);
int				ps_find_min_index(long *stack, int size);

void			ps_print_stack(t_memory *memory);

#endif

/*
**	MEMO:
**	  3 val > 2-3 op	OK
**	  5 val > 12 op		KO
**	100 val	> 700 op	KO
**	500 val	> 5300 op	KO
*/
