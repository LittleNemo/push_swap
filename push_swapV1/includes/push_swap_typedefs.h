/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_typedefs.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/11 11:32:12 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/20 14:54:04 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_TYPEDEFS_H
# define PUSH_SWAP_TYPEDEFS_H

typedef struct	s_stack
{
	long			*value;
	int				size;
}				t_stack;

typedef struct	s_memory
{
	t_stack			stack_a;
	t_stack			stack_b;
	int				size_a;
	int				size_b;
	short			opts;
	unsigned int	nb_inst;
}				t_memory;

typedef enum	e_opts
{
	PS_OPTS_V,
	PS_OPTS_S,
	PS_OPTS_O
}				t_opts;

#endif
