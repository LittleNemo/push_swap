/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 07:31:08 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/12 17:20:24 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				ps_cnt_sorted(long *stack, int size)
{
	int				i;

	i = -1;
	while (++i < size - 1)
		if (stack[i] > stack[i + 1])
			return (i);
	return (i);
}

int				ps_issorted(t_stack stack)
{
	int				i;

	i = -1;
	while (++i < stack.size - 1)
		if (stack.value[i] > stack.value[i + 1])
			return (0);
	return (1);
}

int				ps_isrsorted(long *stack, int size)
{
	int				i;

	i = -1;
	while (++i < size - 1)
		if (stack[i] < stack[i + 1])
			return (0);
	return (1);
}

int				ps_find_max_index(t_stack stack)
{
	int				i;
	int				max_index;
	long			max_value;

	i = -1;
	max_index = 0;
	max_value = stack.value[0];
	while (++i < stack.size)
		if (stack.value[i] > max_value)
		{
			max_index = i;
			max_value = stack.value[i];
		}
	return (max_index);
}

int				ps_find_min_index(long *stack, int size)
{
	int				i;
	int				min_index;
	long			min_value;

	i = -1;
	min_index = 0;
	min_value = stack[0];
	while (++i < size)
		if (stack[i] < min_value)
		{
			min_index = i;
			min_value = stack[i];
		}
	return (min_index);
}
