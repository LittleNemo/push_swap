/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_op_rrotate.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 13:16:24 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/12 17:02:32 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			ps_op_rrotate_a(t_memory *memory)
{
	int				i;
	long			tmp;

	if (!memory->stack_a.size)
		return ;
	i = memory->stack_a.size - 1;
	tmp = memory->stack_a.value[i++];
	while (--i)
		memory->stack_a.value[i] = memory->stack_a.value[i - 1];
	memory->stack_a.value[0] = tmp;
	ft_printf("rra\n");
	if (memory->opts & (1 << PS_OPTS_V))
		ps_print_stack(memory);
	memory->nb_inst++;
}

void			ps_op_rrotate_b(t_memory *memory)
{
	int				i;
	long			tmp;

	if (!memory->stack_b.size)
		return ;
	i = memory->stack_b.size - 1;
	tmp = memory->stack_b.value[i++];
	while (--i)
		memory->stack_b.value[i] = memory->stack_b.value[i - 1];
	memory->stack_b.value[0] = tmp;
	ft_printf("rrb\n");
	if (memory->opts & (1 << PS_OPTS_V))
		ps_print_stack(memory);
	memory->nb_inst++;
}

void			ps_op_rrotate_r(t_memory *memory)
{
	int				i;
	long			tmp;

	if (memory->stack_a.size > 0 && memory->stack_b.size > 0)
	{
		i = memory->stack_a.size - 1;
		tmp = memory->stack_a.value[i++];
		while (--i)
			memory->stack_a.value[i] = memory->stack_a.value[i - 1];
		memory->stack_a.value[0] = tmp;
		i = memory->stack_b.size - 1;
		tmp = memory->stack_b.value[i++];
		while (--i)
			memory->stack_b.value[i] = memory->stack_b.value[i - 1];
		memory->stack_b.value[0] = tmp;
		ft_printf("rrr\n");
		if (memory->opts & (1 << PS_OPTS_V))
			ps_print_stack(memory);
		memory->nb_inst++;
	}
	else if (!memory->stack_a.size)
		ps_op_rrotate_b(memory);
	else
		ps_op_rrotate_a(memory);
}
