/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/02 15:20:07 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/12 16:27:13 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				main(int ac, char **av)
{
	t_memory		memory;

	ps_get_opts(&memory, &ac, av + 1);
	ps_error((ac == 1), 0);
	if (ac == 2)
	{
		av = ft_strsplit(av[1], ' ');
		ac = 0;
		while (av[ac])
			ac++;
		ps_get_values(&memory, ac, av);
	}
	else
		ps_get_values(&memory, ac - 1, av + 1);
	ps_sort(&memory);
	if (memory.opts & (1 << PS_OPTS_O))
		ft_printf("%@Sorting done in |%d| instructions.%@\n", \
		GREEN, memory.nb_inst, EOC);
	return (0);
}
