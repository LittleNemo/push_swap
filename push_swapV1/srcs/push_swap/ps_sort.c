/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_sort.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/11 16:40:22 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/12 18:04:08 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_sort_three(t_memory *memory)
{
	int				max_index;

	if (memory->stack_a.size == 1)
		return ;
	else if ((memory->stack_a.size == 2) && (memory->stack_a.value[0] > memory->stack_a.value[1]))
		ps_op_swap_a(memory);
	else if (memory->stack_a.size == 3)
	{
		max_index = ps_find_max_index(memory->stack_a);
		if (max_index == 0)
			ps_op_rotate_a(memory);
		if (max_index == 1)
			ps_op_rrotate_a(memory);
		if (memory->stack_a.value[0] > memory->stack_a.value[1])
			ps_op_swap_a(memory);
	}
}

/*static int		ps_find_index(long *stack, long value)
{
	int				i;

	i = 0;
	while (stack[i] != value)
		i++;
	return (i);
}


void			ft_debug_hey_banner3(void)
{
	printf("##     ## ######## ##    ##\n");
	printf("##     ## ##        ##  ##\n");
	printf("##     ## ##         ####\n");
	printf("######### ######      ##\n");
	printf("##     ## ##          ##\n");
	printf("##     ## ##          ##\n");
	printf("##     ## ########    ##\n\n");
}

void			ps_sort_infinity_2(t_memory *memory, long lim, long old_lim)
{
	double			median_a;
	double			median_b;
	long			lim_b;

	if ((median_b = ps_find_median(memory->stack_b, memory.size_b)) == -1)
		return ;
	lim_b = memory->stack_b[memory.size_b - 1];
	ps_sort_unfill_b(memory, lim_b, median_b);
	if (memory.size_b >= 2)
		ps_sort_infinity_2(memory, memory->stack_a[0], lim);
	else
	{
		if (memory->stack_b[0] < memory->stack_b[1])
			ps_op_swap_b(memory);
		ps_op_push_a(memory);
		ps_op_push_a(memory);
		ps_op_rotate_a(memory);
		ps_op_rotate_a(memory);
	}
//==============================================================================
	if ((ps_find_index(memory->stack_a, old_lim) + 1) >= 2)
	{
		if (memory->stack_a[0] > memory->stack_a[1])
			ps_op_swap_a(memory);	
		ps_op_rotate_a(memory);
		ps_op_rotate_a(memory);
		return ;
	}
	else
		median_a = ps_find_median(memory->stack_a, ps_find_index(memory->stack_a, old_lim) + 1);
//==============================================================================
printf("median_a: %.f\n", median_a); 
printf("lim_a: %ld\n", lim);
printf("median_b: %.f\n", median_b); 
printf("lim_b: %ld\n", lim_b);
printf("old_lim: %ld\n\n", old_lim); 
//==============================================================================
	while (memory->stack_a[0] != old_lim)
		if (memory->stack_a[0] < median_a)
			ps_op_push_b(memory);
		else
			ps_op_rotate_a(memory);
	if (memory->stack_a[0] < median_a)
		ps_op_push_b(memory);
	else
		ps_op_rotate_a(memory);

//==============================================================================
	while (memory->stack_a[memory.size_a - 1] > median_a)
		ps_op_rrotate_a(memory);
	while (memory.size_b)
		ps_op_push_a(memory);
//==============================================================================
	
	if (memory.size_b >= 2)
		ps_sort_infinity(memory, old_lim);
	else
	{
		if (memory->stack_b[0] < memory->stack_b[1])
			ps_op_swap_b(memory);
		ps_op_push_a(memory);
		ps_op_push_a(memory);
		ps_op_rotate_a(memory);
		ps_op_rotate_a(memory);
		return ;
	}

}

static void		ps_sort_unfill_b(t_memory *memory)
{
	long			median_b;
	int				size;

	median_b = ps_find_median(memory->stack_b, memory.size_b);
	size = memory.size_b;

	if (memory.size_b == 2)
	{
		if (memory->stack_b[0] < memory->stack_b[1])
			ps_op_swap_b(memory);
		ps_op_push_a(memory);
		ps_op_push_a(memory);
		ps_op_rotate_a(memory);
		ps_op_rotate_a(memory);
		return 2;
	}
	while (memory.size_b > (size / 2))
		if (memory->stack_b[0] > median_b)
			ps_op_push_a(memory);
		else
			ps_op_rotate_b(memory);
	if (memory->stack_b[0] >= median_b)
		ps_op_push_a(memory);
	ps_sort_unfill_b(memory);
	ps_sort_infinity(memory->stack_a, size / 2);

}*/

void			ps_sort_infinity(t_memory *memory, t_stack *stack_1, t_stack *stack_2, int size)
{
	double			median;
	int				cnt;

	median = ps_find_median(stack_1);
	cnt = 0;
	while (cnt < size / 2)
		if (stack_1->value[0] < median)
			ps_op_push_b(memory);
		else
		{
			ps_op_rotate_a(memory);
			cnt++;
		}
}

//	ps_sort_infinity_2(memory, memory->stack_a[0], lim_a);
/*
**	MEMO:
**	1-	calc median
**	2-	value < median ==> stack B
**		value > median ==> top stack A
**	3-	top stack A ==> bot stack A
**	4-	stack B ==> bot stack A
**	5-	if not sorted: recall
*/

void			ps_sort(t_memory *memory)
{
	if (ps_issorted(memory->stack_a))
		return ;
	else if (memory->stack_a.size <= 3)
		ps_sort_three(memory);
	else
		ps_sort_infinity(memory, &memory->stack_a, &memory->stack_b, memory->stack_a.size);
}
