/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_get_opts.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/16 11:31:59 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/16 11:39:20 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_erase_opts(int *ac, char **av, int *i, int *j)
{
	*j = -1;
	*ac -= *i;
	if (*i)
		while (++*j < *ac)
			av[*j] = av[*j + *i];
}

void			ps_get_opts(t_memory *memory, int *ac, char **av)
{
	int				i;
	int				j;

	memory->opts = 0;
	i = -1;
	while (av[++i] && av[i][0] == '-' && ft_str_islower(av[i] + 1))
	{
		j = 0;
		while (av[i][++j])
		{
			if (av[i][j] == 'v')
				memory->opts |= (1 << PS_OPTS_V);
			else if (av[i][j] == 's')
				memory->opts |= (1 << PS_OPTS_S);
			else if (av[i][j] == 'o')
				memory->opts |= (1 << PS_OPTS_O);
			else
				ps_error(6, av[i][1]);
		}
	}
	ps_erase_opts(ac, av, &i, &j);
}
