/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_find_median.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/25 08:51:58 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/12 18:02:58 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_find_median_sort(t_stack *stack, double *tab)
{
	int				i;
	double			tmp;

	i = -1;
	while (++i < stack->size)
		tab[i] = stack->value[i];
	i = -1;
	while (++i < (stack->size - 1))
		if (tab[i] > tab[i + 1])
		{
			tmp = tab[i + 1];
			tab[i + 1] = tab[i];
			tab[i] = tmp;
			i = -1;
		}
}

double			ps_find_median(t_stack *stack)
{
	double			median;
	double			*tab;

	if (!stack->size)
		return (-1);
	if (!(tab = (double *)malloc(sizeof(tab) * stack->size)))
		ps_error(5, 0);
	ps_find_median_sort(stack, tab);
	if (!(stack->size % 2))
		median = ((tab[(stack->size / 2) - 1] + tab[(stack->size / 2)]) / 2);
	else
		median = tab[stack->size / 2];
	free(tab);
	return (median);
}
