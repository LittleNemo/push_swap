/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_op_push.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 15:37:28 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/12 17:48:58 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			ps_op_push_a(t_memory *memory)
{
	int				i;

	if (!memory->stack_b.size)
		return ;
	i = memory->stack_a.size + 1;
	while (--i)
		memory->stack_a.value[i] = memory->stack_a.value[i - 1];
	memory->stack_a.value[0] = memory->stack_b.value[0];
	i = -1;
	while (++i < memory->stack_b.size)
		memory->stack_b.value[i] = memory->stack_b.value[i + 1];
	memory->stack_a.size++;
	memory->stack_b.size--;
	ft_printf("pa\n");
	if (memory->opts & (1 << PS_OPTS_V))
		ps_print_stack(memory);
	memory->nb_inst++;
}

void			ps_op_push_b(t_memory *memory)
{
	int				i;

	if (!memory->stack_a.size)
		return ;
	i = memory->stack_b.size + 1;
	while (--i)
		memory->stack_b.value[i] = memory->stack_b.value[i - 1];
	memory->stack_b.value[0] = memory->stack_a.value[0];
	i = -1;
	while (++i < memory->stack_a.size)
		memory->stack_a.value[i] = memory->stack_a.value[i + 1];
	memory->stack_a.size--;
	memory->stack_b.size++;
	ft_printf("pb\n");
	if (memory->opts & (1 << PS_OPTS_V))
		ps_print_stack(memory);
	memory->nb_inst++;
}
