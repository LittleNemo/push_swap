/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_op_swap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/11 17:32:37 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/12 17:02:32 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			ps_op_swap_a(t_memory *memory)
{
	long			tmp;

	if (memory->stack_a.size < 2)
		return ;
	tmp = memory->stack_a.value[0];
	memory->stack_a.value[0] = memory->stack_a.value[1];
	memory->stack_a.value[1] = tmp;
	ft_printf("sa\n");
	if (memory->opts & (1 << PS_OPTS_V))
		ps_print_stack(memory);
	memory->nb_inst++;
}

void			ps_op_swap_b(t_memory *memory)
{
	long			tmp;

	if (memory->stack_b.size < 2)
		return ;
	tmp = memory->stack_b.value[0];
	memory->stack_b.value[0] = memory->stack_b.value[1];
	memory->stack_b.value[1] = tmp;
	ft_printf("sb\n");
	if (memory->opts & (1 << PS_OPTS_V))
		ps_print_stack(memory);
	memory->nb_inst++;
}

void			ps_op_swap_s(t_memory *memory)
{
	long			tmp;

	if (memory->stack_a.size >= 2 && memory->stack_b.size >= 2)
	{
		tmp = memory->stack_a.value[0];
		memory->stack_a.value[0] = memory->stack_a.value[1];
		memory->stack_a.value[1] = tmp;
		tmp = memory->stack_b.value[0];
		memory->stack_b.value[0] = memory->stack_b.value[1];
		memory->stack_b.value[1] = tmp;
		ft_printf("ss\n");
		if (memory->opts & (1 << PS_OPTS_V))
			ps_print_stack(memory);
		memory->nb_inst++;
	}
	else if (memory->stack_a.size < 2)
		ps_op_swap_b(memory);
	else
		ps_op_swap_a(memory);
}
