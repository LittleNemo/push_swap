/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_op_rotate.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 12:57:13 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/12 17:02:32 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			ps_op_rotate_a(t_memory *memory)
{
	int				i;
	long			tmp;

	if (!memory->stack_a.size)
		return ;
	i = -1;
	tmp = memory->stack_a.value[0];
	while (++i < memory->stack_a.size - 1)
		memory->stack_a.value[i] = memory->stack_a.value[i + 1];
	memory->stack_a.value[i] = tmp;
	ft_printf("ra\n");
	if (memory->opts & (1 << PS_OPTS_V))
		ps_print_stack(memory);
	memory->nb_inst++;
}

void			ps_op_rotate_b(t_memory *memory)
{
	int				i;
	long			tmp;

	if (!memory->stack_b.size)
		return ;
	i = -1;
	tmp = memory->stack_b.value[0];
	while (++i < memory->stack_b.size - 1)
		memory->stack_b.value[i] = memory->stack_b.value[i + 1];
	memory->stack_b.value[i] = tmp;
	ft_printf("rb\n");
	if (memory->opts & (1 << PS_OPTS_V))
		ps_print_stack(memory);
	memory->nb_inst++;
}

void			ps_op_rotate_r(t_memory *memory)
{
	int				i;
	long			tmp;

	if (memory->stack_a.size > 0 && memory->stack_b.size > 0)
	{
		i = -1;
		tmp = memory->stack_a.value[0];
		while (++i < memory->stack_a.size - 1)
			memory->stack_a.value[i] = memory->stack_a.value[i + 1];
		memory->stack_a.value[i] = tmp;
		i = -1;
		tmp = memory->stack_b.value[0];
		while (++i < memory->stack_b.size - 1)
			memory->stack_b.value[i] = memory->stack_b.value[i + 1];
		memory->stack_b.value[i] = tmp;
		ft_printf("rr\n");
		if (memory->opts & (1 << PS_OPTS_V))
			ps_print_stack(memory);
		memory->nb_inst++;
	}
	else if (!memory->stack_a.size)
		ps_op_rotate_b(memory);
	else
		ps_op_rotate_a(memory);
}
