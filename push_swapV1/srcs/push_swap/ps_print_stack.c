/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_print_stack.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/11 18:31:22 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/12 17:06:39 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_print_stack_a(t_stack stack, int opts, int cnt)
{
	int				i;

	i = 0;
	if (opts & (1 << PS_OPTS_S))
		ft_printf("%@Stack A %@(%d)\t%@:%@", CYAN, cnt, stack.size, CYAN, EOC);
	else
		ft_printf("%@Stack A \t:%@", CYAN, cnt, stack.size, EOC);
	if (stack.size > 0)
		ft_printf(" %@%d%@", GREEN, stack.value[i], EOC);
	while (++i < stack.size && stack.size > 0)
		if (stack.value[i] < stack.value[i - 1])
			ft_printf(" %@%d%@", RED, stack.value[i], EOC);
		else
			ft_printf(" %@%d%@", GREEN, stack.value[i], EOC);
	ft_printf("\n");
}

static void		ps_print_stack_b(t_stack stack, int opts, int cnt)
{
	int				i;

	i = -1;
	if (opts & (1 << PS_OPTS_S))
		ft_printf("%@Stack B %@(%d)\t%@:%@", CYAN, cnt, stack.size, CYAN, EOC);
	else
		ft_printf("%@Stack B \t:%@", CYAN, cnt, stack.size, EOC);
	while (++i < stack.size)
		ft_printf(" %@%d%@", YELLOW, stack.value[i], EOC);
	ft_printf("\n");
}

void			ps_print_stack(t_memory *memory)
{
	int				cnt;

	cnt = GREEN;
	if (memory->stack_b.size)
		cnt = RED;
	ps_print_stack_a(memory->stack_a, memory->opts, cnt);
	ps_print_stack_b(memory->stack_b, memory->opts, cnt);
	ft_printf("\n");
}
