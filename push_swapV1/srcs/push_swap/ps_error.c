/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/09 16:19:19 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/14 17:10:01 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				ps_error(int error, int c)
{
	if (error == 0)
		return (0);
	else if (error == 1)
		ft_fprintf(stderr, "%s\n%s\n", PS_USAGE, PS_USAGE_ALT);
	else if (error == 2)
		ft_fprintf(stderr, "%s\n", PS_ERROR_DOUBLE);
	else if (error == 3)
		ft_fprintf(stderr, "%s\n", PS_ERROR_NOT_INT);
	else if (error == 4)
		ft_fprintf(stderr, "%s\n", PS_ERROR_NOT_NUM);
	else if (error == 5)
		ft_fprintf(stderr, "%s\n", PS_ERROR_MALLOC);
	else if (error == 6)
		ft_fprintf(stderr, "%s%c\n", PS_ERROR_ARGS, c);
	else
		ft_fprintf(stderr, "%s\n", PS_ERROR_UNKNOW);
	exit(EXIT_FAILURE);
}
