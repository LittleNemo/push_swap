/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_get_values.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/16 11:34:42 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/12 16:54:53 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		ps_value_isvalid(t_memory *memory, char **values, int index)
{
	int				i;

	if (!(values[index][0] == '-' && ft_str_isdigit(values[index] + 1)) && \
		!ft_str_isdigit(values[index]))
	{
		free(memory->stack_a.value);
		free(memory->stack_b.value);
		return (4);
	}
	if (ft_atol(values[index]) < INT_MIN || ft_atol(values[index]) > INT_MAX)
	{
		free(memory->stack_a.value);
		free(memory->stack_b.value);
		return (3);
	}
	i = 0;
	while (values[i] && i != index)
		if (ft_strequ(values[i++], values[index]))
		{
			free(memory->stack_a.value);
			free(memory->stack_b.value);
			return (2);
		}
	return (0);
}

void			ps_get_values(t_memory *memory, int size, char **values)
{
	int				i;

	if (!(memory->stack_a.value = (long*)malloc(sizeof(long) * size)) || \
		!(memory->stack_b.value = (long*)malloc(sizeof(long) * size)))
		ps_error(5, 0);
	memory->stack_a.size = size;
	memory->stack_b.size = 0;
	i = 0;
	while (i < size)
	{
		if (!ps_error(ps_value_isvalid(memory, values, i), 0))
			memory->stack_a.value[i] = ft_atoi(values[i]);
		memory->stack_b.value[i++] = 0;
	}
}
