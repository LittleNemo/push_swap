/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_op_rrotate.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/15 14:55:44 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/25 07:41:17 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void			ck_op_rrotate_a(t_memory *memory)
{
	int				i;
	long			tmp;

	if (!memory->size_a)
		return ;
	i = memory->size_a - 1;
	tmp = memory->stack_a[i++];
	while (--i)
		memory->stack_a[i] = memory->stack_a[i - 1];
	memory->stack_a[0] = tmp;
	if (memory->opts & (1 << CK_OPTS_V))
		ck_print_stack(memory, "rra");
	memory->nb_inst++;
}

void			ck_op_rrotate_b(t_memory *memory)
{
	int				i;
	long			tmp;

	if (!memory->size_b)
		return ;
	i = memory->size_b - 1;
	tmp = memory->stack_b[i++];
	while (--i)
		memory->stack_b[i] = memory->stack_b[i - 1];
	memory->stack_b[0] = tmp;
	if (memory->opts & (1 << CK_OPTS_V))
		ck_print_stack(memory, "rrb");
	memory->nb_inst++;
}

void			ck_op_rrotate_r(t_memory *memory)
{
	int				i;
	long			tmp;

	if (memory->size_a > 0 && memory->size_b > 0)
	{
		i = memory->size_a - 1;
		tmp = memory->stack_a[i++];
		while (--i)
			memory->stack_a[i] = memory->stack_a[i - 1];
		memory->stack_a[0] = tmp;
		i = memory->size_b - 1;
		tmp = memory->stack_b[i++];
		while (--i)
			memory->stack_b[i] = memory->stack_b[i - 1];
		memory->stack_b[0] = tmp;
		if (memory->opts & (1 << CK_OPTS_V))
			ck_print_stack(memory, "rrr");
		memory->nb_inst++;
	}
	else if (!memory->size_a)
		ck_op_rrotate_b(memory);
	else
		ck_op_rrotate_a(memory);
}
