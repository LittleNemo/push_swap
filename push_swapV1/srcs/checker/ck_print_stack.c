/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_print_stack.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/15 14:52:00 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/25 07:41:25 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void		ck_print_stack_a(long *stack, int size, int opts, int cnt)
{
	int				i;

	i = 0;
	if (opts & (1 << CK_OPTS_S))
		ft_printf("%@Stack A %@(%d)\t%@:%@", CYAN, cnt, size, CYAN, EOC);
	else
		ft_printf("%@Stack A \t:%@", CYAN, cnt, size, EOC);
	if (size > 0)
		ft_printf(" %@%d%@", GREEN, stack[i], EOC);
	while (++i < size)
		if (stack[i] < stack[i - 1])
			ft_printf(" %@%d%@", RED, stack[i], EOC);
		else
			ft_printf(" %@%d%@", GREEN, stack[i], EOC);
	ft_printf("\n");
}

static void		ck_print_stack_b(long *stack, int size, int opts, int cnt)
{
	int				i;

	i = -1;
	if (opts & (1 << CK_OPTS_S))
		ft_printf("%@Stack B %@(%d)\t%@:%@", CYAN, cnt, size, CYAN, EOC);
	else
		ft_printf("%@Stack B \t:%@", CYAN, cnt, size, EOC);
	while (++i < size)
		ft_printf(" %@%d%@", YELLOW, stack[i], EOC);
	ft_printf("\n");
}

void			ck_print_stack(t_memory *memory, char *inst)
{
	int				cnt;

	ft_printf("%@Instruction\t: %@%s%@\n", LIGHT_BLUE, ORANGE, inst, EOC);
	cnt = GREEN;
	if (memory->size_b)
		cnt = RED;
	ck_print_stack_a(memory->stack_a, memory->size_a, memory->opts, cnt);
	ck_print_stack_b(memory->stack_b, memory->size_b, memory->opts, cnt);
	ft_printf("\n");
}
