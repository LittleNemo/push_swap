/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_reading.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/15 15:06:06 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/17 07:34:36 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void		ck_do_inst(t_memory *memory, char *line)
{
	if (ft_strequ(line, "pa"))
		ck_op_push_a(memory);
	else if (ft_strequ(line, "pb"))
		ck_op_push_b(memory);
	else if (ft_strequ(line, "ra"))
		ck_op_rotate_a(memory);
	else if (ft_strequ(line, "rb"))
		ck_op_rotate_b(memory);
	else if (ft_strequ(line, "rr"))
		ck_op_rotate_r(memory);
	else if (ft_strequ(line, "rra"))
		ck_op_rrotate_a(memory);
	else if (ft_strequ(line, "rrb"))
		ck_op_rrotate_b(memory);
	else if (ft_strequ(line, "rrr"))
		ck_op_rrotate_r(memory);
	else if (ft_strequ(line, "sa"))
		ck_op_swap_a(memory);
	else if (ft_strequ(line, "sb"))
		ck_op_swap_b(memory);
	else if (ft_strequ(line, "ss"))
		ck_op_swap_s(memory);
	else
		ck_error(7, 0);
}

void			ck_reading(t_memory *memory)
{
	char			*line;

	while (get_next_line(stdin->_file, &line))
		ck_do_inst(memory, line);
	if (ck_issorted(memory->stack_a, memory->size_a) && !memory->size_b)
		ft_printf("%@OK%@\n", GREEN, EOC);
	else
		ft_printf("%@KO%@\n", RED, EOC);
}
