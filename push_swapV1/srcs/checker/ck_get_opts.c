/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_get_opts.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/16 11:49:01 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/16 11:53:02 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void		ck_erase_opts(int *ac, char **av, int *i, int *j)
{
	*j = -1;
	*ac -= *i;
	if (*i)
		while (++*j < *ac)
			av[*j] = av[*j + *i];
}

void			ck_get_opts(t_memory *memory, int *ac, char **av)
{
	int				i;
	int				j;

	memory->opts = 0;
	i = -1;
	while (av[++i] && av[i][0] == '-' && ft_str_islower(av[i] + 1))
	{
		j = 0;
		while (av[i][++j])
		{
			if (av[i][j] == 'v')
				memory->opts |= (1 << CK_OPTS_V);
			else if (av[i][j] == 's')
				memory->opts |= (1 << CK_OPTS_S);
			else if (av[i][j] == 'o')
				memory->opts |= (1 << CK_OPTS_O);
			else
				ck_error(6, av[i][1]);
		}
	}
	ck_erase_opts(ac, av, &i, &j);
}
