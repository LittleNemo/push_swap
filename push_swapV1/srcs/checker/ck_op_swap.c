/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_op_swap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/15 14:57:00 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/25 07:42:23 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void			ck_op_swap_a(t_memory *memory)
{
	long			tmp;

	if (memory->size_a < 2)
		return ;
	tmp = memory->stack_a[0];
	memory->stack_a[0] = memory->stack_a[1];
	memory->stack_a[1] = tmp;
	if (memory->opts & (1 << CK_OPTS_V))
		ck_print_stack(memory, "sa");
	memory->nb_inst++;
}

void			ck_op_swap_b(t_memory *memory)
{
	long			tmp;

	if (memory->size_b < 2)
		return ;
	tmp = memory->stack_b[0];
	memory->stack_b[0] = memory->stack_b[1];
	memory->stack_b[1] = tmp;
	if (memory->opts & (1 << CK_OPTS_V))
		ck_print_stack(memory, "sb");
	memory->nb_inst++;
}

void			ck_op_swap_s(t_memory *memory)
{
	long			tmp;

	if (memory->size_a >= 2 && memory->size_b >= 2)
	{
		tmp = memory->stack_a[0];
		memory->stack_a[0] = memory->stack_a[1];
		memory->stack_a[1] = tmp;
		tmp = memory->stack_b[0];
		memory->stack_b[0] = memory->stack_b[1];
		memory->stack_b[1] = tmp;
		if (memory->opts & (1 << CK_OPTS_V))
			ck_print_stack(memory, "ss");
		memory->nb_inst++;
	}
	else if (memory->size_a < 2)
		ck_op_swap_b(memory);
	else
		ck_op_swap_a(memory);
}
