/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_op_push.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/15 14:53:08 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/25 07:41:21 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void			ck_op_push_a(t_memory *memory)
{
	int				i;

	if (!memory->size_b)
		return ;
	i = memory->size_a + 1;
	while (--i)
		memory->stack_a[i] = memory->stack_a[i - 1];
	memory->stack_a[0] = memory->stack_b[0];
	i = -1;
	while (++i < memory->size_b)
		memory->stack_b[i] = memory->stack_b[i + 1];
	memory->size_a++;
	memory->size_b--;
	if (memory->opts & (1 << CK_OPTS_V))
		ck_print_stack(memory, "pa");
	memory->nb_inst++;
}

void			ck_op_push_b(t_memory *memory)
{
	int				i;

	if (!memory->size_a)
		return ;
	i = memory->size_b + 1;
	while (--i)
		memory->stack_b[i] = memory->stack_b[i - 1];
	memory->stack_b[0] = memory->stack_a[0];
	i = -1;
	while (++i < memory->size_a)
		memory->stack_a[i] = memory->stack_a[i + 1];
	memory->size_a--;
	memory->size_b++;
	if (memory->opts & (1 << CK_OPTS_V))
		ck_print_stack(memory, "pb");
	memory->nb_inst++;
}
