/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/07 11:28:00 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/17 07:22:36 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

int				main(int ac, char **av)
{
	t_memory		memory;

	ck_get_opts(&memory, &ac, av + 1);
	ck_error((ac == 1), 0);
	if (ac == 2)
	{
		av = ft_strsplit(av[1], ' ');
		ac = 0;
		while (av[ac])
			ac++;
		ck_get_values(&memory, ac, av);
	}
	else
		ck_get_values(&memory, ac - 1, av + 1);
	ck_reading(&memory);
	if (memory.opts & (1 << CK_OPTS_O))
		ft_printf("%@Sorting done in |%d| instructions.%@\n", \
		GREEN, memory.nb_inst, EOC);
	return (0);
}
