/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_op_rotate.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/15 14:54:23 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/25 07:41:19 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void			ck_op_rotate_a(t_memory *memory)
{
	int				i;
	long			tmp;

	if (!memory->size_a)
		return ;
	i = -1;
	tmp = memory->stack_a[0];
	while (++i < memory->size_a - 1)
		memory->stack_a[i] = memory->stack_a[i + 1];
	memory->stack_a[i] = tmp;
	if (memory->opts & (1 << CK_OPTS_V))
		ck_print_stack(memory, "rb");
	memory->nb_inst++;
}

void			ck_op_rotate_b(t_memory *memory)
{
	int				i;
	long			tmp;

	if (!memory->size_b)
		return ;
	i = -1;
	tmp = memory->stack_b[0];
	while (++i < memory->size_b - 1)
		memory->stack_b[i] = memory->stack_b[i + 1];
	memory->stack_b[i] = tmp;
	if (memory->opts & (1 << CK_OPTS_V))
		ck_print_stack(memory, "rb");
	memory->nb_inst++;
}

void			ck_op_rotate_r(t_memory *memory)
{
	int				i;
	long			tmp;

	if (memory->size_a > 0 && memory->size_b > 0)
	{
		i = -1;
		tmp = memory->stack_a[0];
		while (++i < memory->size_a - 1)
			memory->stack_a[i] = memory->stack_a[i + 1];
		memory->stack_a[i] = tmp;
		i = -1;
		tmp = memory->stack_b[0];
		while (++i < memory->size_b - 1)
			memory->stack_b[i] = memory->stack_b[i + 1];
		memory->stack_b[i] = tmp;
		if (memory->opts & (1 << CK_OPTS_V))
			ck_print_stack(memory, "rr");
		memory->nb_inst++;
	}
	else if (!memory->size_a)
		ck_op_rotate_b(memory);
	else
		ck_op_rotate_a(memory);
}
