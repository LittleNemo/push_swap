/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_get_values.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/16 11:48:58 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/24 13:13:13 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static int		ck_value_isvalid(t_memory *memory, char **values, int index)
{
	int				i;

	if (!(values[index][0] == '-' && ft_str_isdigit(values[index] + 1)) && \
		!ft_str_isdigit(values[index]))
	{
		free(memory->stack_a);
		free(memory->stack_b);
		return (4);
	}
	if (ft_atol(values[index]) < INT_MIN || ft_atol(values[index]) > INT_MAX)
	{
		free(memory->stack_a);
		free(memory->stack_b);
		return (3);
	}
	i = 0;
	while (values[i] && i != index)
		if (ft_strequ(values[i++], values[index]))
		{
			free(memory->stack_a);
			free(memory->stack_b);
			return (2);
		}
	return (0);
}

void			ck_get_values(t_memory *memory, int size, char **values)
{
	int				i;

	if (!(memory->stack_a = (long*)malloc(sizeof(memory->stack_a) * size)) || \
		!(memory->stack_b = (long*)malloc(sizeof(memory->stack_b) * size)))
		ck_error(5, 0);
	memory->size_a = size;
	memory->size_b = 0;
	memory->nb_inst = 0;
	i = 0;
	while (i < size)
	{
		if (!ck_error(ck_value_isvalid(memory, values, i), 0))
			memory->stack_a[i] = ft_atoi(values[i]);
		memory->stack_b[i++] = 0;
	}
}
