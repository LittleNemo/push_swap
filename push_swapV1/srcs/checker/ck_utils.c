/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 07:34:09 by lbrangie          #+#    #+#             */
/*   Updated: 2018/05/17 07:34:34 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

int				ck_issorted(long *stack, int size)
{
	int				i;

	i = -1;
	while (++i < size - 1)
		if (stack[i] > stack[i + 1])
			return (0);
	return (1);
}
