# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/09 13:29:53 by lbrangie          #+#    #+#              #
#    Updated: 2018/07/18 15:45:20 by lbrangie         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

################################################################################
##                                   TARGET                                   ##
################################################################################
NAME				=		libftprintf.a


################################################################################
##                                   SOURCES                                  ##
################################################################################
# ============================== DIRECTORIES ============================== #
SRCS_DIR			=		srcs/
SRCS_DIR_PRINTF		=		ft_printf/
SRCS_DIR_LIBFT		=		libft/

# ================================= FILES ================================= #
FILES_PRINTF		=		ft_printf.c				\
							ft_vprintf.c			\
							ptf_buffer.c			\
							ptf_get_flags.c			\
							ptf_put_arbs.c			\
							ptf_put_bin.c			\
							ptf_put_char.c			\
							ptf_put_dec.c			\
							ptf_put_hex.c			\
							ptf_put_ldec.c			\
							ptf_put_loct.c			\
							ptf_put_ludec.c			\
							ptf_put_mhex.c			\
							ptf_put_oct.c			\
							ptf_put_ptr.c			\
							ptf_put_ret.c			\
							ptf_put_str.c			\
							ptf_put_udec.c			\
							ptf_put_wchar.c			\
							ptf_put_wstr.c			\
							ptf_select_cast.c		\
							ptf_singleton_colors.c	\
							ptf_singleton_hash.c	\
							ptf_utils.c				\
							ptf_write_num.c


# ================================ SOURCES ================================ #
SRCS_PRINTF_TMP		=		$(addprefix $(SRCS_DIR_PRINTF), $(FILES_PRINTF))
SRCS_PRINTF			=		$(addprefix $(SRCS_DIR), $(SRCS_PRINTF_TMP))


################################################################################
##                                   OBJECTS                                  ##
################################################################################
# ============================== DIRECTORIES ============================== #
OBJS_DIR			=		objs/
OBJS_DIR_PRINTF		=		ft_printf/
OBJS_DIR_LIBFT		=		libft/

# ================================ OBJECTS ================================ #
OBJS_PRINTF_TMP		=		$(addprefix $(OBJS_DIR_PRINTF), $(FILES_PRINTF:.c=.o))
OBJS_PRINTF			=		$(addprefix $(OBJS_DIR), $(OBJS_PRINTF_TMP))


################################################################################
##                                  COMPILING                                 ##
################################################################################
# ============================== COMPILATOR =============================== #
CC					=		gcc
CFLAGS				+=		-Wall -Wextra -Werror -O3

# =============================== INCLUDES ================================ #
INC_DIR				=		includes/
INC_FILE			=		ft_printf.h				\
							ft_printf_defines.h		\
							ft_printf_typedefs.h
INC_COMP			=		-I$(INC_DIR)

# =============================== LIBRARIES =============================== #
LIB_NAME			=		ft
LIB_PATH			=		./srcs/libft/
LIB_SRC_NAME		=		$(shell make -C $(LIB_PATH) print-SRCS)
LIB_SRC				=		$(addprefix $(LIB_PATH), $(LIB_SRC_NAME))
LIB_COMP			=		-Llib$(LIB_NAME) -l$(LIB_NAME)
LIB_ARCV			=		lib$(LIB_NAME).a
AR					=		ar rc
RAN					=		ranlib

# ================================ SLAVES ================================= #
MAKESLV				=		make -C


################################################################################
##                                  DELETION                                  ##
################################################################################
RM					=		rm -f
RM_DIR				=		rm -rfd


################################################################################
##                                    RULES                                   ##
################################################################################

all					:		$(NAME)

proper				:
	@$(MAKESLV) . all
	@$(MAKESLV) . clean

$(OBJS_DIR)%.o		:		$(SRCS_DIR)%.c
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC_COMP)

$(OBJS_DIR)			:
	@mkdir -p $@
	@mkdir -p $@/$(OBJS_DIR_PRINTF)

$(LIB_PATH)$(LIB_ARCV)	: $(LIB_SRC)
	@$(MAKESLV) $(LIB_PATH) all

$(NAME)				:		$(OBJS_DIR) $(OBJS_PRINTF) $(LIB_PATH)$(LIB_ARCV)
	@$(AR) $@ $(OBJS_PRINTF) $(LIB_PATH)$(OBJS_DIR)*/*
	@$(RAN) $@

clean				:
	@$(RM_DIR) $(OBJS_DIR)
	@$(MAKESLV) $(LIB_PATH) clean

fclean				:		clean
	@$(RM) $(NAME)
	@$(MAKESLV) $(LIB_PATH) fclean

re					:
	@$(MAKESLV) . fclean
	@$(MAKESLV) . all

norme				:		$(INC_DIR) $(SRCS_DIR)$(SRCS_DIR_PRINTF)
	@norminette $^ | grep -B 1 "Warning\|Error" || true
	@$(MAKESLV) $(LIB_PATH) norme

print-%				:
	@echo $($*)

.PHONY				:		all proper clean fclean re norme
