/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_read_inst.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/02 11:44:56 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/25 18:30:44 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void		ck_do(char *line, t_stack **st_a, t_stack **st_b, short opts)
{
	int				ret;
	size_t			len;

	ret = -1;
	len = ft_strlen(line);
	if (len == 2)
	{
		if (line[0] == 'p')
			ret = ck_do_push(line[1], st_a, st_b);
		else if (line[0] == 's')
			ret = ck_do_swap(line[1], st_a, st_b);
		else if (line[0] == 'r')
			ret = ck_do_rotate(line[1], st_a, st_b);
	}
	else if (len == 3 && ft_strnequ(line, "rr", 2))
		ret = ck_do_rrotate(line[2], st_a, st_b);
	if (ret == -1)
		ck_error(4, 0);
	else if (opts & (1 << CK_OPTS_V))
		ck_print_stacks(line, *st_a, *st_b);
}

int				ck_read_inst(t_stack **st_a, t_stack **st_b, short opts)
{
	char			*line;
	int				nb_inst;

	line = NULL;
	nb_inst = 0;
	while (get_next_line(stdin->_file, &line))
	{
		ck_do(line, st_a, st_b, opts);
		free(line);
		nb_inst++;
	}
	free(line);
	return (nb_inst);
}
