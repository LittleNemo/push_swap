/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_do_inst_rotate.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/03 13:17:17 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/25 17:38:38 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void		ck_do_rotate_stack(t_stack **stack)
{
	t_stack			*tmp;

	if (ck_lstlen(*stack) >= 2)
	{
		tmp = *stack;
		while (tmp->nxt)
			tmp = tmp->nxt;
		tmp->nxt = *stack;
		*stack = (*stack)->nxt;
		tmp->nxt->nxt = NULL;
	}
}

int				ck_do_rotate(char stack, t_stack **st_a, t_stack **st_b)
{
	if (!ft_strchr("abr", stack))
		return (-1);
	if (stack == 'a' || stack == 'r')
		ck_do_rotate_stack(st_a);
	if (stack == 'b' || stack == 'r')
		ck_do_rotate_stack(st_b);
	return (0);
}
