/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/28 15:28:51 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/28 16:01:14 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

int				ck_error(int error, char c)
{
	if (error == 0)
		return (0);
	else if (error == 1)
		ft_fprintf(stderr, "%s\n", CK_USAGE);
	else if (error == 2)
		ft_fprintf(stderr, "%s%c\n", CK_ERROR_ARGS, c);
	else if (error == 3)
		ft_fprintf(stderr, "%s\n", CK_ERROR_DOUBLE);
	else if (error == 4)
		ft_fprintf(stderr, "%s\n", CK_ERROR_FORMAT);
	else if (error == 5)
		ft_fprintf(stderr, "%s\n", CK_ERROR_MALLOC);
	else if (error == 6)
		ft_fprintf(stderr, "%s\n", CK_ERROR_NOT_INT);
	else if (error == 7)
		ft_fprintf(stderr, "%s\n", CK_ERROR_NOT_NUM);
	else
		ft_fprintf(stderr, "%s\n", CK_ERROR_UNKNOW);
	exit(EXIT_FAILURE);
}
