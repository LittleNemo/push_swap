/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/03 14:38:33 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/03 14:22:29 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void			ck_free_list(t_stack *list)
{
	t_stack			*tmp;

	while (list)
	{
		tmp = list;
		list = list->nxt;
		free(tmp);
	}
}

size_t			ck_lstlen(t_stack *list)
{
	size_t			len;

	len = 0;
	while (list)
	{
		len++;
		list = list->nxt;
	}
	return (len);
}

t_stack			*ck_lst_findlast(t_stack *list)
{
	if (!list)
		return (list);
	while (list->nxt)
		list = list->nxt;
	return (list);
}
