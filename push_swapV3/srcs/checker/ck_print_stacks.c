/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_print_stacks.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/03 13:47:32 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/27 13:23:33 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void		ck_print_stack_b(t_stack *st_b, int color)
{
	t_stack			*tmp;
	size_t			size;

	tmp = st_b;
	size = ck_lstlen(st_b);
	ft_printf("%@Stack B %@(%zu)%@\t:%@", CYAN, color, size, CYAN, EOC);
	while (tmp)
	{
		ft_printf(" %@%ld%@", YELLOW, tmp->value, EOC);
		tmp = tmp->nxt;
	}
	ft_putchar('\n');
}

static void		ck_print_stack_a(t_stack *st_a, int color)
{
	t_stack			*tmp;
	t_stack			*tmp2;
	size_t			size;

	tmp = st_a;
	tmp2 = st_a;
	size = ck_lstlen(st_a);
	ft_printf("%@Stack A %@(%zu)%@\t:%@", CYAN, color, size, CYAN, EOC);
	if (tmp)
	{
		ft_printf(" %@%d%@", GREEN, tmp->value, EOC);
		tmp = tmp->nxt;
		while (tmp)
		{
			if (tmp->value < tmp2->value)
				ft_printf(" %@%ld%@", RED, tmp->value, EOC);
			else
				ft_printf(" %@%ld%@", GREEN, tmp->value, EOC);
			tmp = tmp->nxt;
			tmp2 = tmp2->nxt;
		}
	}
	ft_putchar('\n');
}

void			ck_print_stacks(char *inst, t_stack *st_a, t_stack *st_b)
{
	int				color;

	ft_printf("%@Instruction\t: %@%s%@\n", LIGHT_BLUE, ORANGE, inst, EOC);
	color = GREEN;
	if (st_b)
		color = RED;
	ck_print_stack_a(st_a, color);
	ck_print_stack_b(st_b, color);
	ft_printf("\033[80A");
	usleep(35000);
	ft_printf("\033[2J");
	ft_putchar('\n');
}

/*
*/
