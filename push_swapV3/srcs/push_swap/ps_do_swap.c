/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_do_swap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/25 18:01:55 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/25 18:01:57 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				ps_ndo_sa(t_stack **st_a, t_stack **st_b, int n)
{
	while (n--)
		if (ps_do("sa", st_a, st_b) == -1)
			return (-1);
	return (0);
}

int				ps_ndo_sb(t_stack **st_a, t_stack **st_b, int n)
{
	while (n--)
		if (ps_do("sb", st_a, st_b) == -1)
			return (-1);
	return (0);
}

int				ps_ndo_ss(t_stack **st_a, t_stack **st_b, int n)
{
	while (n--)
		if (ps_do("ss", st_a, st_b) == -1)
			return (-1);
	return (0);
}

static void		ps_do_swap_stack(t_stack **stack)
{
	t_stack			*tmp;

	if (ps_lstlen(*stack) >= 2)
	{
		tmp = (*stack)->nxt;
		(*stack)->nxt = tmp->nxt;
		tmp->nxt = *stack;
		*stack = tmp;
	}
}

int				ps_do_swap(char stack, t_stack **st_a, t_stack **st_b)
{
	if (!ft_strchr("abs", stack))
		return (-1);
	if (stack == 'a' || stack == 's')
		ps_do_swap_stack(st_a);
	if (stack == 'b' || stack == 's')
		ps_do_swap_stack(st_b);
	return (0);
}
