/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/06 14:08:06 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/03 17:13:17 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				ps_do(char *op, t_stack **st_a, t_stack **st_b)
{
	int				ret;
	size_t			len;

	ret = -1;
	len = ft_strlen(op);
	if (len == 2)
	{
		if (op[0] == 'p')
			ret = ps_do_push(op[1], st_a, st_b);
		else if (op[0] == 's')
			ret = ps_do_swap(op[1], st_a, st_b);
		else if (op[0] == 'r')
			ret = ps_do_rotate(op[1], st_a, st_b);
	}
	else if (len == 3 && ft_strnequ(op, "rr", 2))
		ret = ps_do_rrotate(op[2], st_a, st_b);
	ft_putstr_endl(op);
	return (ret);
}

int				main(int ac, char **av)
{
	t_stack			*st_a;
	t_stack			*st_b;
	long			*tab;

	st_a = NULL;
	st_b = NULL;
	ps_error(!(ac - 1), 0);
	st_a = ps_get_values(av + 1);
	tab = ps_get_tab(av + 1);
	ps_sort(&st_a, &st_b, tab, ps_lstlen(st_a));
	ps_free_list(st_a);
	ps_free_list(st_b);
	free(tab);
	return (0);
}
