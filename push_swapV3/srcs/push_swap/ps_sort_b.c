/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_sort_b.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/01 18:57:55 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/01 19:01:17 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				ps_search_value(t_stack **st_b, long tab)
{
	t_stack			*tmp;
	int				cnt;

	cnt = 0;
	tmp = *st_b;
	while (tmp)
	{
		if (tmp->value == tab)
			return (cnt);
		tmp = tmp->nxt;
		cnt++;
	}
	return (0);
}

void			ps_sort_hundred(t_stack **st_a, t_stack **st_b, long *tab)
{
	int				i;
	int				cnt;

	i = ps_lstlen(*st_b) - 1;
	while (i >= 0)
	{
		cnt = ps_search_value(st_b, tab[i]);
		if (cnt <= ps_lstlen(*st_b) / 2)
			ps_ndo_rb(st_a, st_b, cnt);
		else
			ps_ndo_rrb(st_a, st_b, ps_lstlen(*st_b) - cnt);
		ps_do("pa", st_a, st_b);
		i--;
	}
}

int				ps_is_value_sorted(t_stack **st, double tab)
{
	t_stack			*tmp;

	tmp = *st;
	while (tmp)
	{
		if (tmp->value <= tab)
			return (0);
		tmp = tmp->nxt;
	}
	return (1);
}

void			ps_to_sb_one(t_stack **st_a, t_stack **st_b, long tab)
{
	while (!ps_is_value_sorted(st_a, tab))
		if ((*st_a)->value <= tab)
			ps_do("pb", st_a, st_b);
		else
			ps_do("ra", st_a, st_b);
}

void			ps_to_sb_mlt(t_stack **st_a, t_stack **st_b, long tab)
{
	int				count;

	count = 0;
	while (!ps_is_value_sorted(st_a, tab))
		if ((*st_a)->value <= tab)
			ps_do("pb", st_a, st_b);
		else
		{
			ps_do("ra", st_a, st_b);
			count++;
		}
	if (count < (ps_lstlen(*st_a) / 2))
		ps_ndo_rra(st_a, st_b, count);
	else
		ps_ndo_ra(st_a, st_b, ps_lstlen(*st_a) - count);
}
