/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_find_med.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/10 12:56:45 by lbrangie          #+#    #+#             */
/*   Updated: 2018/07/10 15:44:09 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_find_median_sort(t_stack *stack, double *tab, int size)
{
	int				i;
	double			tmp;

	i = -1;
	while (++i < size)
	{
		tab[i] = stack->value;
		stack = stack->nxt;
	}
	i = -1;
	while (++i < size - 1)
		if (tab[i] > tab[i + 1])
		{
			tmp = tab[i + 1];
			tab[i + 1] = tab[i];
			tab[i] = tmp;
			i = -1;
		}
}

double			ps_find_med(t_stack *stack, int size)
{
	double			med;
	double			*tab;

	if (!stack)
		return (-1);
	if (!(tab = (double *)malloc(sizeof(tab) * (unsigned long)size)))
		ps_error(4, 0);
	ps_find_median_sort(stack, tab, size);
	if (size % 2)
		med = tab[size / 2];
	else
		med = ((tab[(size / 2) - 1] + tab[size / 2]) / 2);
	free(tab);
	return (med);
}
