/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_do_rrotate.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/25 18:01:27 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/25 18:01:28 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				ps_ndo_rra(t_stack **st_a, t_stack **st_b, int n)
{
	while (n--)
		if (ps_do("rra", st_a, st_b) == -1)
			return (-1);
	return (0);
}

int				ps_ndo_rrb(t_stack **st_a, t_stack **st_b, int n)
{
	while (n--)
		if (ps_do("rrb", st_a, st_b) == -1)
			return (-1);
	return (0);
}

int				ps_ndo_rrr(t_stack **st_a, t_stack **st_b, int n)
{
	while (n--)
		if (ps_do("rrr", st_a, st_b) == -1)
			return (-1);
	return (0);
}

static void		ps_do_rrotate_stack(t_stack **stack)
{
	t_stack			*tmp;

	if (ps_lstlen(*stack) >= 2)
	{
		tmp = *stack;
		while (tmp->nxt->nxt)
			tmp = tmp->nxt;
		tmp->nxt->nxt = *stack;
		*stack = tmp->nxt;
		tmp->nxt = NULL;
	}
}

int				ps_do_rrotate(char stack, t_stack **st_a, t_stack **st_b)
{
	if (!ft_strchr("abr", stack))
		return (-1);
	if (stack == 'a' || stack == 'r')
		ps_do_rrotate_stack(st_a);
	if (stack == 'b' || stack == 'r')
		ps_do_rrotate_stack(st_b);
	return (0);
}
