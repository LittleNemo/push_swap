/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_get_tab.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 14:29:14 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/28 15:02:20 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_sort_tab(long *tab, int size)
{
	int				i;
	long			tmp;

	i = -1;
	while (++i < size - 1)
		if (tab[i] > tab[i + 1])
		{
			tmp = tab[i + 1];
			tab[i + 1] = tab[i];
			tab[i] = tmp;
			i = -1;
		}
}

static int		ps_size_tab(char **tab)
{
	int				i;

	i = 0;
	while (tab[i])
		i++;
	return (i);
}

static char		*ps_args_to_str(char **av)
{
	char			*str;
	char			*tmp;
	int				i;

	i = 0;
	tmp = NULL;
	if (!(str = ft_strjoin(av[i], " ")))
		return (NULL);
	while (av[++i])
	{
		tmp = str;
		if (!(str = ft_strjoin(tmp, av[i])))
			return (NULL);
		ft_strdel(&tmp);
		tmp = str;
		if (!(str = ft_strjoin(tmp, " ")))
			return (NULL);
		ft_strdel(&tmp);
	}
	return (str);
}

long			*ps_get_tab(char **av)
{
	char			*args;
	char			**c_tab;
	long			*n_tab;
	int				size;
	int				i;

	args = ps_args_to_str(av);
	c_tab = ft_strusplit(args, PS_SEPARATOR);
	size = ps_size_tab(c_tab);
	if (!(n_tab = (long *)malloc(sizeof(n_tab) * size)))
		ps_error(4, 0);
	i = -1;
	while (c_tab[++i])
		n_tab[i] = ft_atol(c_tab[i]);
	ps_sort_tab(n_tab, size);
	ft_strdel(&args);
	ft_freetab((void**)c_tab);
	return (n_tab);
}
