/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 13:41:50 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/01 15:01:12 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				ps_issorted(t_stack *stack)
{
	while (stack)
	{
		if (stack->nxt)
			if (stack->value > stack->nxt->value)
				return (0);
		stack = stack->nxt;
	}
	return (1);
}

void			ps_free_list(t_stack *list)
{
	t_stack			*tmp;

	while (list)
	{
		tmp = list;
		list = list->nxt;
		free(tmp);
	}
}

int				ps_find_max_link(t_stack *stack)
{
	int				curr_link;
	int				max_link;
	t_stack			*tmp;

	tmp = stack;
	curr_link = 0;
	max_link = 0;
	while (stack)
	{
		if (stack->value > tmp->value)
		{
			tmp = stack;
			max_link = curr_link;
		}
		stack = stack->nxt;
		curr_link++;
	}
	return (max_link);
}

int				ps_lstlen(t_stack *list)
{
	int				len;

	len = 0;
	while (list)
	{
		len++;
		list = list->nxt;
	}
	return (len);
}

t_stack			*ps_lst_findlast(t_stack *list)
{
	if (!list)
		return (list);
	while (list->nxt)
		list = list->nxt;
	return (list);
}
