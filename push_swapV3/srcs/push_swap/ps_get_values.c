/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_get_values.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/06 16:38:09 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/03 18:15:17 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static t_stack	*ps_get_values_update(char *str)
{
	t_stack			*new;

	if (!(new = (t_stack *)malloc(sizeof(*new))))
		ps_error(4, 0);
	new->value = ft_atol(str);
	new->nxt = NULL;
	return (new);
}

static int		ps_value_isvalid(char *str, t_stack *list)
{
	long			value;

	if (!str)
		return (1);
	if (!(str[0] == '-' && ft_str_isdigit(str + 1)) && !ft_str_isdigit(str))
		return (6);
	value = ft_atol(str);
	if (value < INT_MIN || INT_MAX < value)
		return (5);
	while (list)
	{
		if (list->value == value)
			return (3);
		list = list->nxt;
	}
	return (0);
}

static char		*ps_args_to_str(char **av)
{
	char			*str;
	char			*tmp;
	int				i;

	i = 0;
	tmp = NULL;
	if (!(str = ft_strjoin(av[i], " ")))
		return (NULL);
	while (av[++i])
	{
		tmp = str;
		if (!(str = ft_strjoin(tmp, av[i])))
			return (NULL);
		ft_strdel(&tmp);
		tmp = str;
		if (!(str = ft_strjoin(tmp, " ")))
			return (NULL);
		ft_strdel(&tmp);
	}
	return (str);
}

t_stack			*ps_get_values(char **av)
{
	char			*args;
	char			**tab;
	t_stack			*list;
	t_stack			*tmp;
	int				i;

	args = ps_args_to_str(av);
	tab = ft_strusplit(args, PS_SEPARATOR);
	list = NULL;
	tmp = list;
	i = 0;
	if (!ps_error(ps_value_isvalid(tab[i], list), 0))
		list = ps_get_values_update(tab[i]);
	while (tab[++i])
	{
		if (!ps_error(ps_value_isvalid(tab[i], list), 0))
			tmp = ps_get_values_update(tab[i]);
		ps_lst_findlast(list)->nxt = tmp;
	}
	ft_strdel(&args);
	ft_freetab((void **)tab);
	return (list);
}
