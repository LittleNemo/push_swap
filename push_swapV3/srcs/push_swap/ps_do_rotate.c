/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_do_rotate.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/25 18:01:10 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/25 18:01:16 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				ps_ndo_ra(t_stack **st_a, t_stack **st_b, int n)
{
	while (n--)
		if (ps_do("ra", st_a, st_b) == -1)
			return (-1);
	return (0);
}

int				ps_ndo_rb(t_stack **st_a, t_stack **st_b, int n)
{
	while (n--)
		if (ps_do("rb", st_a, st_b) == -1)
			return (-1);
	return (0);
}

int				ps_ndo_rr(t_stack **st_a, t_stack **st_b, int n)
{
	while (n--)
		if (ps_do("rr", st_a, st_b) == -1)
			return (-1);
	return (0);
}

static void		ps_do_rotate_stack(t_stack **stack)
{
	t_stack			*tmp;

	if (ps_lstlen(*stack) >= 2)
	{
		tmp = *stack;
		while (tmp->nxt)
			tmp = tmp->nxt;
		tmp->nxt = *stack;
		*stack = (*stack)->nxt;
		tmp->nxt->nxt = NULL;
	}
}

int				ps_do_rotate(char stack, t_stack **st_a, t_stack **st_b)
{
	if (!ft_strchr("abr", stack))
		return (-1);
	if (stack == 'a' || stack == 'r')
		ps_do_rotate_stack(st_a);
	if (stack == 'b' || stack == 'r')
		ps_do_rotate_stack(st_b);
	return (0);
}
