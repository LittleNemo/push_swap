/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_sort.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 15:08:49 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/01 18:59:20 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_unfill(t_stack **st_a, t_stack **st_b, int med, int sorted)
{
	while ((ps_lstlen(*st_a) - sorted != med) && ps_lstlen(*st_b))
		ps_do("pa", st_a, st_b);
}

static void		ps_sort_core(t_stack **st_a, t_stack **st_b, int sorted)
{
	if (ps_lstlen(*st_a) - sorted == 3)
		ps_sort_three(st_a, st_b);
	if (ps_lstlen(*st_a) - sorted == 2 && (*st_a)->value > (*st_a)->nxt->value)
		ps_do("sa", st_a, st_b);
}

static void		ps_sort_medians(t_stack **st_a, int *med, int size, int sorted)
{
	med[0] = size - sorted;
	med[1] = ps_lstlen(*st_a) - sorted;
	med[2] = med[1] / 2;
}

static int		ps_cnt_sorted(t_stack **st_a, long *tab, int size, int start)
{
	t_stack			*here;
	t_stack			*forward;
	int				a_sorted;
	int				n;
	int				i;

	here = *st_a;
	a_sorted = size - ps_lstlen(*st_a);
	while (here && (a_sorted < size))
	{
		n = a_sorted;
		i = 1;
		forward = here;
		while (forward && (size > a_sorted) && tab[n] == forward->value)
		{
			n++;
			i++;
			forward = forward->nxt;
			if ((forward && !forward->nxt) && tab[size - 1] == forward->value)
				return (i);
		}
		here = here->nxt;
		a_sorted++;
	}
	return (start);
}

void			ps_sort(t_stack **st_a, t_stack **st_b, long *tab, int size)
{
	int				meds[3];
	int				sorted;

	if (ps_issorted(*st_a) && !(*st_b))
		return ;
	sorted = ps_cnt_sorted(st_a, tab, size, 0);
	ps_sort_medians(st_a, meds, size, sorted);
	if (ps_lstlen(*st_a) - sorted == 4 || ps_lstlen(*st_a) == 5)
		meds[2] = 3;
	if (ps_lstlen(*st_a) - sorted > 3)
	{
		if (sorted)
			ps_to_sb_mlt(st_a, st_b, tab[meds[0] - meds[2] - 1]);
		else
			ps_to_sb_one(st_a, st_b, tab[meds[0] - meds[2] - 1]);
	}
	if (ps_lstlen(*st_a) - sorted > 3)
		ps_sort(st_a, st_b, tab, size);
	ps_sort_core(st_a, st_b, sorted);
	if (ps_lstlen(*st_a) == ps_cnt_sorted(st_a, tab, size, 0) && meds[0] <= 100)
		ps_sort_hundred(st_a, st_b, tab);
	if (ps_lstlen(*st_a) - sorted != meds[1])
		ps_unfill(st_a, st_b, meds[1], sorted);
	if (ps_lstlen(*st_a) != ps_cnt_sorted(st_a, tab, size, sorted))
		ps_sort(st_a, st_b, tab, size);
}
