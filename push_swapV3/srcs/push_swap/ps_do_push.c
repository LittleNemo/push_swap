/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_do_push.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/25 17:58:58 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/25 17:59:02 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				ps_ndo_pa(t_stack **st_a, t_stack **st_b, int n)
{
	while (n--)
		if (ps_do("pa", st_a, st_b) == -1)
			return (-1);
	return (0);
}

int				ps_ndo_pb(t_stack **st_a, t_stack **st_b, int n)
{
	while (n--)
		if (ps_do("pb", st_a, st_b) == -1)
			return (-1);
	return (0);
}

static void		ps_do_push_stack(t_stack **src, t_stack **dst)
{
	t_stack			*tmp;

	if (*src)
	{
		tmp = *src;
		*src = (*src)->nxt;
		tmp->nxt = *dst;
		*dst = tmp;
	}
}

int				ps_do_push(char stack, t_stack **st_a, t_stack **st_b)
{
	if (!ft_strchr("ab", stack))
		return (-1);
	else if (stack == 'a')
		ps_do_push_stack(st_b, st_a);
	else if (stack == 'b')
		ps_do_push_stack(st_a, st_b);
	return (0);
}
