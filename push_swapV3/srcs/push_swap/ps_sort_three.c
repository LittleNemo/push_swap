/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_sort_three.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/29 11:33:31 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/29 11:52:51 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_sort_only_three(t_stack **st_a, t_stack **st_b)
{
	int				max_link;

	if (ps_lstlen(*st_a) == 1)
		return ;
	else if (ps_lstlen(*st_a) == 2 && (*st_a)->value > (*st_a)->nxt->value)
		ps_do("sa", st_a, st_b);
	else
	{
		max_link = ps_find_max_link(*st_a);
		if (max_link == 0)
			ps_do("ra", st_a, st_b);
		if (max_link == 1)
			ps_do("rra", st_a, st_b);
		if ((*st_a)->value > (*st_a)->nxt->value)
			ps_do("sa", st_a, st_b);
	}
}

static int		ps_which_case(t_stack **st_a)
{
	if ((*st_a)->value < (*st_a)->nxt->value && \
			(*st_a)->value < (*st_a)->nxt->nxt->value &&\
			(*st_a)->nxt->value < (*st_a)->nxt->nxt->value)
		return (0);
	if ((*st_a)->value < (*st_a)->nxt->value && \
			(*st_a)->value < (*st_a)->nxt->nxt->value &&\
			(*st_a)->nxt->value > (*st_a)->nxt->nxt->value)
		return (1);
	if ((*st_a)->value > (*st_a)->nxt->value && \
			(*st_a)->value < (*st_a)->nxt->nxt->value &&\
			(*st_a)->nxt->value < (*st_a)->nxt->nxt->value)
		return (2);
	if ((*st_a)->value < (*st_a)->nxt->value && \
			(*st_a)->value > (*st_a)->nxt->nxt->value &&\
			(*st_a)->nxt->value > (*st_a)->nxt->nxt->value)
		return (3);
	if ((*st_a)->value > (*st_a)->nxt->value && \
			(*st_a)->value > (*st_a)->nxt->nxt->value &&\
			(*st_a)->nxt->value < (*st_a)->nxt->nxt->value)
		return (4);
	if ((*st_a)->value > (*st_a)->nxt->value && \
			(*st_a)->value > (*st_a)->nxt->nxt->value &&\
			(*st_a)->nxt->value > (*st_a)->nxt->nxt->value)
		return (5);
	return (0);
}

static void		ps_sort_sup_three(t_stack **st_a, t_stack **st_b)
{
	int				type;

	type = ps_which_case(st_a);
	if (type == 0)
		return ;
	if (type == 1 || type == 3)
	{
		ps_do("ra", st_a, st_b);
		ps_do("sa", st_a, st_b);
		ps_do("rra", st_a, st_b);
	}
	if (type == 4 || type == 5)
	{
		ps_do("sa", st_a, st_b);
		ps_do("ra", st_a, st_b);
	}
	if (type == 2 || type == 3 || type == 4 || type == 5)
		ps_do("sa", st_a, st_b);
	if (type == 4 || type == 5)
		ps_do("rra", st_a, st_b);
	if (type == 5)
		ps_do("sa", st_a, st_b);
}

void			ps_sort_three(t_stack **st_a, t_stack **st_b)
{
	size_t			len;

	len = ps_lstlen(*st_a);
	if (len <= 3)
		ps_sort_only_three(st_a, st_b);
	if (len > 3)
		ps_sort_sup_three(st_a, st_b);
}
