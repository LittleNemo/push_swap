/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_defines.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/28 12:00:11 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/25 17:53:24 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_DEFINES_H
# define PUSH_SWAP_DEFINES_H

# ifndef PS_SEPARATOR
#  define PS_SEPARATOR		"\t\n\v\f\r "
# endif

# ifndef PS_USAGE
#  define PS_USAGE			"usage: ./push_swap value1 value2 ..."
# endif

# ifndef PS_ERROR_ARGS
#  define PS_ERROR_ARGS		"push_swap: illegal option -- "
# endif
# ifndef PS_ERROR_DOUBLE
#  define PS_ERROR_DOUBLE	"Error (push_swap): a value appears twice"
# endif
# ifndef PS_ERROR_MALLOC
#  define PS_ERROR_MALLOC	"Error (push_swap): malloc failed"
# endif
# ifndef PS_ERROR_NOT_INT
#  define PS_ERROR_NOT_INT	"Error (push_swap): a value isn't an int"
# endif
# ifndef PS_ERROR_NOT_NUM
#  define PS_ERROR_NOT_NUM	"Error (push_swap): a value isn't numerical"
# endif
# ifndef PS_ERROR_UNKNOW
#  define PS_ERROR_UNKNOW	"Error (push_swap): unknow"
# endif

#endif
