/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/28 13:21:23 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/25 17:33:22 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_H
# define CHECKER_H

# include <limits.h>
# include <stdlib.h>
# include <stdio.h>
# include "../srcs/ft_printf/includes/ft_printf.h"
# include "checker_defines.h"
# include "checker_typedefs.h"

short			ck_get_opts(char **av, short *opts);
t_stack			*ck_get_values(char **av);

int				ck_read_inst(t_stack **st_a, t_stack **st_b, short opts);
int				ck_do_push(char inst, t_stack **st_a, t_stack **st_b);
int				ck_do_swap(char inst, t_stack **st_a, t_stack **st_b);
int				ck_do_rotate(char inst, t_stack **st_a, t_stack **st_b);
int				ck_do_rrotate(char inst, t_stack **st_a, t_stack **st_b);

void			ck_print_stacks(char *inst, t_stack *st_a, t_stack *st_b);
int				ck_error(int error, char c);

void			ck_free_list(t_stack *list);
size_t			ck_lstlen(t_stack *list);
t_stack			*ck_lst_findlast(t_stack *list);

#endif
