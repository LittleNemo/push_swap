/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/28 11:57:49 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/01 19:03:19 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <limits.h>
# include <stdlib.h>
# include <stdio.h>
# include "../srcs/ft_printf/includes/ft_printf.h"
# include "push_swap_defines.h"
# include "push_swap_typedefs.h"

int				ps_error(int error, char c);

long			*ps_get_tab(char **av);
t_stack			*ps_get_values(char **av);

int				ps_do(char *op, t_stack **st_a, t_stack **st_b);
int				ps_do_push(char inst, t_stack **st_a, t_stack **st_b);
int				ps_ndo_pa(t_stack **st_a, t_stack **st_b, int n);
int				ps_ndo_pb(t_stack **st_a, t_stack **st_b, int n);
int				ps_do_swap(char inst, t_stack **st_a, t_stack **st_b);
int				ps_ndo_sa(t_stack **st_a, t_stack **st_b, int n);
int				ps_ndo_sb(t_stack **st_a, t_stack **st_b, int n);
int				ps_ndo_ss(t_stack **st_a, t_stack **st_b, int n);
int				ps_do_rotate(char inst, t_stack **st_a, t_stack **st_b);
int				ps_ndo_ra(t_stack **st_a, t_stack **st_b, int n);
int				ps_ndo_rb(t_stack **st_a, t_stack **st_b, int n);
int				ps_ndo_rr(t_stack **st_a, t_stack **st_b, int n);
int				ps_do_rrotate(char inst, t_stack **st_a, t_stack **st_b);
int				ps_ndo_rra(t_stack **st_a, t_stack **st_b, int n);
int				ps_ndo_rrb(t_stack **st_a, t_stack **st_b, int n);
int				ps_ndo_rrr(t_stack **st_a, t_stack **st_b, int n);

int				ps_search_value(t_stack **st_b, long tab);
void			ps_sort_hundred(t_stack **st_a, t_stack **st_b, long *tab);
int				ps_is_value_sorted(t_stack **st, double tab);
void			ps_to_sb_one(t_stack **st_a, t_stack **st_b, long tab);
void			ps_to_sb_mlt(t_stack **st_a, t_stack **st_b, long tab);
void			ps_sort_three(t_stack **st_a, t_stack **st_b);
void			ps_sort(t_stack **st_a, t_stack **st_b, long *tab, int size);

double			ps_find_med(t_stack *stack, int size);
int				ps_issorted(t_stack *stack);
void			ps_free_list(t_stack *list);
int				ps_find_max_link(t_stack *stack);
int				ps_lstlen(t_stack *stack);
t_stack			*ps_lst_findlast(t_stack *list);

#endif
