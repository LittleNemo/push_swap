/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_typedefs.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/28 13:23:47 by lbrangie          #+#    #+#             */
/*   Updated: 2018/07/03 15:38:08 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_TYPEDEFS_H
# define CHECKER_TYPEDEFS_H

typedef enum	e_opts
{
	CK_OPTS_V,
	CK_OPTS_O
}				t_opts;

typedef struct	s_stack
{
	long			value;
	struct s_stack	*nxt;
}				t_stack;

#endif
