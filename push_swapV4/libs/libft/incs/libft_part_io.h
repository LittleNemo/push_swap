/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_part_io.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 15:33:26 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:13:23 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PART_IO_H
# define LIBFT_PART_IO_H

# include <unistd.h>
# include <fcntl.h>
# include <limits.h>
# include "libft_defines.h"
# include "libft_typedefs.h"
# include "libft_part_string.h"

/*
**	These functions are part of the io portion of the libft
*/
int				ft_putchar(char c);
int				ft_putchar_endl(char c);
int				ft_putchar_endl_fd(char c, int fd);
int				ft_putchar_fd(char c, int fd);
int				ft_putfile(const char *file_name);
int				ft_putfile_fd(const char *file_name, int fd);
int				ft_putstr(const char *s);
int				ft_putstr_endl(const char *s);
int				ft_putstr_endl_fd(const char *s, int fd);
int				ft_putstr_fd(const char *s, int fd);
void			ft_puttab(char **tab);
void			ft_puttab_fd(char **tab, int fd);

int				get_next_line(const int fd, char **line);

#endif
