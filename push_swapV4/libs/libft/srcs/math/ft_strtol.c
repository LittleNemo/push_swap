/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtol.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 17:11:58 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/26 19:39:17 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_math.h"
#include <stdio.h>

static int		ft_get_value(char c, int base)
{
	if (ft_isdigit(c))
		c -= '0';
	else if (ft_islower(c))
		c = c - 'a' + 10;
	else if (ft_isupper(c))
		c = c - 'A' + 10;
	else
		return (-1);
	if (c >= base)
		return (-1);
	return (c);
}

static t_bool	ft_process(long *ret, char **str, int base, t_bool neg)
{
	long			cutoff;
	int				cutlim;
	char			c;

	cutoff = (neg) ? -(unsigned long)LONG_MIN : LONG_MAX;
	cutlim = cutoff % (long)base;
	cutoff /= (unsigned long)base;
	while ((c = *(*str)++))
	{
		if ((c = ft_get_value(c, base)) == -1)
			return (false);
		if (*ret > cutoff || (*ret == cutoff && c > cutlim))
			return (false);
		*ret = *ret * base + c;
	}
	return (true);
}

static t_bool	ft_get_base(char **str, int *base, \
		t_bool pref_hex, t_bool pref_bin)
{
	if (*base == 0 && pref_hex)
		*base = 16;
	else if (*base == 0 && pref_bin)
		*base = 2;
	else if (*base == 0 && *str[0] == '0')
		*base = 8;
	else if (*base == 0)
		*base = 10;
	return (true);
}

static t_bool	ft_get_info(char **str, int *base, t_bool *neg)
{
	t_bool			pref_hex;
	t_bool			pref_bin;

	pref_hex = false;
	pref_bin = false;
	(*str) += ft_strspn(*str, " \t\n\v\f\r");
	if (*str[0] == '+')
		(*str)++;
	else if (*str[0] == '-' && (*neg = true))
		(*str)++;
	if (*str[0] == '0' && ft_tolower((*str)[1]) == 'x')
		pref_hex = true;
	else if (*str[0] == '0' && ft_tolower((*str)[1]) == 'b')
		pref_bin = true;
	if (ft_get_base(str, base, pref_hex, pref_bin) && (*base < 2 || *base > 36))
		return (false);
	if (*base == 2 || *base == 16)
		*str += 2;
	else if (*base == 8)
		(*str)++;
	if (ft_get_value(**str, *base) == -1 && !pref_hex && !pref_bin)
		return (false);
	return (true);
}

long			ft_strtol(const char *str, char **endptr, int base)
{
	char			*s;
	long			ret;
	t_bool			neg;

	s = (char *)str;
	ret = 0;
	neg = false;
	if (!ft_get_info(&s, &base, &neg))
	{
		if (endptr)
			*endptr = (char *)s;
		return (0);
	}
	if (!ft_process(&ret, &s, base, neg))
	{
		if (endptr)
			*endptr = (char *)s;
		if (neg)
			return (LONG_MIN);
		return (LONG_MAX);
	}
	if (neg)
		return (-ret);
	return (ret);
}
