/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrcasechr.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 18:58:59 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:28:35 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_strrcasechr() function locates the last occurence of c (converted
**	to a char) in the string s, ignoring the case. The null-terminateing
**	character is concidered to be part of s. This means if c is '\0', the
**	function will locate the terminating '\0'.
**
** RETURN VALUES
**	- ft_strrcasechr() returns a pointer to the located character, or NULL if c
**	does not appear in s.
*/

char			*ft_strrcasechr(const char *s, int c)
{
	char			*save;

	save = NULL;
	while (*s)
	{
		if (ft_tolower(*s) == ft_tolower(c))
			save = (char *)s;
		s++;
	}
	if (ft_tolower(*s) == ft_tolower(c))
		save = (char *)s;
	return (save);
}
