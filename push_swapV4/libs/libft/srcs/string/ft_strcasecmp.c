/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcasecmp.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 18:01:24 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:17:41 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_strcasecmp() function lexicographically compares the
**	null-terminated strings s1 and s2, ignoring the case.
**
** RETURN VALUES
**	- ft_strcasecmp() returns an integer greater than 0 if s1 is greater than
**	s2, less than 0 if s1 is less than s2 or equal to 0 if s1 is equal to s2.
*/

int		ft_strcasecmp(const char *s1, const char *s2)
{
	while (*s1 || *s1)
		if (ft_tolower(*s1++) != ft_tolower(*s2++))
			return (ft_tolower(*--s1) - ft_tolower(*--s2));
	return (0);
}
