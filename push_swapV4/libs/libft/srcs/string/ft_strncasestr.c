/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncasestr.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 19:16:57 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:26:52 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_strncasestr() function locates the first occurence of the
**	null-terminated string needle in the null-terminated string haystack,
**	ignoring the case, where not more than n characters are searched. Character
**	that appears after a '\0' are not searched.
**
** RETURN VALUES
**	- ft_strncasestr() returns haystack if needle is an empty string, NULL if
**	needle occurs nowhere in haystack, otherwise, a pointer to the first
**	character of the first occurence of needle.
*/

char			*ft_strncasestr(const char *haystack, const char *needle, \
		size_t n)
{
	size_t			size;
	const char		*save;

	if (!*needle)
		return ((char *)haystack);
	size = ft_strlen(needle);
	save = haystack;
	haystack += n;
	while ((save = ft_strcasechr(save, *needle)) && ((save + size) <= haystack))
		if (!ft_strncasecmp(save++, needle, size))
			return ((char *)--save);
	return (NULL);
}
