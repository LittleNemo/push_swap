/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 16:45:48 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:20:03 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_strdel() function frees the allocated string *as, and sets the
**	pointer to NULL.
**
** RETURN VALUE
**	- ft_strdel() returns true if the line has succesfully be deleted.
**	Otherwise, false is returned.
*/

t_bool			ft_strdel(char **as)
{
	if (!as || !*as)
		return (false);
	free(*as);
	*as = NULL;
	return (true);
}
