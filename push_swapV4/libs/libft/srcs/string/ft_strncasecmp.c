/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncasecmp.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 18:06:11 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:26:45 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_strncasecmp() function lexicographically compares the
**	null-terminated strings s1 and s2, ignoring the case,  but not more than n
**	characters. Because, there is strings to compare rather than binary data,
**	characters after a '\0' are not compared.
**
** RETURN VALUES
**	- ft_strncasecmp() returns an integer greater than 0 if s1 is greater than
**	s2, less than 0 if s1 is less than s2 or equal to 0 if s1 is equal to s2.
*/

int		ft_strncasecmp(const char *s1, const char *s2, size_t n)
{
	while (n-- && (*s1 || *s1))
		if (ft_tolower(*s1++) != ft_tolower(*s2++))
			return (ft_tolower(*--s1) - ft_tolower(*--s2));
	return (0);
}
