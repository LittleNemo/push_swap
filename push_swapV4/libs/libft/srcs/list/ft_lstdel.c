/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/20 15:21:54 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 17:32:14 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_list.h"

/*
** DESCRIPTION
**	- The ft_lstdel() function frees the mallocated memory of the pointer alst
**	and all the links after. Then, ft_lstdel sets the pointer on alst to NULL.
**
** NOTE
**	- See libft_typedefs.h to see the t_list structure.
*/

void			ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	if (*alst && *del)
	{
		ft_lstdel(&(*alst)->next, del);
		ft_lstdelone(alst, del);
	}
}
