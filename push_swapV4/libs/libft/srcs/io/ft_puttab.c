/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puttab.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 15:38:06 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/26 16:56:09 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_io.h"

/*
** DESCRIPTION
**	- The ft_puttab() function attempts to write the content of the table tab on
**	the standard output, a.k.a. sdtout.
*/

void			ft_puttab(char **tab)
{
	size_t			i;

	if (!tab)
		return ;
	i = 0;
	while (tab[i])
		ft_putstr_endl(tab[i++]);
}
