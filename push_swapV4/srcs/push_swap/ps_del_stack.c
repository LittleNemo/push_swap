#include "push_swap.h"

void			ps_del_stack(t_stk_h *stack)
{
	t_stk			*tmp;

	while (stack->head)
	{
		tmp = stack->head;
		stack->head = stack->head->next;
		free(tmp);
	}
}

void			ps_del_meta(t_meta *meta)
{
	if (meta->r_fd)
		close(meta->r_fd);
	if (meta->w_fd)
	{
		close(meta->w_fd);
		remove(meta->w_file);
	}
	ft_strdel(&meta->r_file);
	ft_strdel(&meta->w_file);
}