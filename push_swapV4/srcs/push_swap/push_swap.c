#include "push_swap.h"

int				main(int ac, char **av)
{
	static t_meta	meta;
	static t_stk_h	stacka;
	static t_stk_h	stackb;


	ps_get_opts(av, &meta);
	if (!(meta->opts & (1 << PS_OPTS_WRITE)))
		ps_error(!(ac - 1 - nb_opts), meta, NULL, NULL);
	ps_get_values(av + 1 + meta->nb_opts, meta, stacka);
	ps_get_s_tab(meta, stacka);
	if (meta->opts & (1 << PS_OPTS_WRITE))
		ps_write_values(meta, stacka);
// =========== TO DO ==========================================================
	ps_sort_stack(meta, stacka, stackb);
// ============================================================================
	ps_del_meta(meta);
	ps_del_stack(stacka);
	ps_del_stack(stackb);
	return (0);
}