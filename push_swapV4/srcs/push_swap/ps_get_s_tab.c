#include "push_swap.h"

static void		ps_quicksort_swap(int *a, int *b)
{
	int				tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}

static void		ps_quicksort_pivot(t_meta *meta, int start, int end)
{
	int				pivot;
	int				i;

	i = start - 1;
	pivot = meta->tab[end];
	while (++i <= end - 1)
		if (meta->tab[i] < pivot)
			swap(&meta->tab[i], &meta->tab[start++]);
	swap(&meta->tab[end], &meta->tab[start]);
	return (start);
}

static void		ps_quicksort(t_meta *meta, int start, int end)
{
	int				pivot;

	if (!(start < end))
		return ;
	pivot = ps_quicksort_pivot(meta, start, end);
	ps_quicksort(meta, start, pivot + 1);
	ps_quicksort(meta, pivot - 1, end);
}

void			ps_get_s_tab(t_meta *meta, t_stk_h *stacka)
{
	size_t			i;
	t_stk			*tmp;

	if (!(meta->tab = (int *)ft_memalloc(sizeof(meta->tab) * (stacka->len - 1))))
		ps_error(PS_ERROR_MALLOC, meta, stacka, NULL);
	tmp = stacka->head;
	i = 0;
	meta->l_tab = stacka->len - 1;
	while (tmp)
	{
		tab[i++] = tmp->value;
		tmp = tmp->next;
	}
	ps_quicksort(meta, 0, stacka->len - 1);
}