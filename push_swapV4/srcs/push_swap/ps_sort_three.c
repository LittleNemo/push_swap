#include "push_swap.h"

static short	ps_find_biggest(t_stk_h *stacka)
{
	int				i;
	int				biggest_val;
	short			biggest_ind;
	t_stk			*tmp;

	i = 3;
	biggest_val = INT_MIN;
	biggest_ind = 0;
	tmp = stacka->head;
	while (tmp && i--)
	{
		if (biggest_val < tmp->value)
		{
			biggest_val = tmp->value;
			biggest_ind = i;
		}
		tmp = tmp->next;
	}
	return (biggest_ind);
}

static void		ps_sort_big(t_meta *meta, t_stk_h *stacka, t_stk_h *stackb)
{
	short			biggest;

	biggest = ps_find_biggest(stacka);
// ==================== case: [3]-[1]-[2]-[4]-[5]-... ====================
// ==================== case: [3]-[2]-[1]-[4]-[5]-... ====================
	if (biggest == 0)
	{
		ps_exec_inst("sa", meta, stacka, stackb);	// -> [1]-[3]-[2]-[4]-[5]-...
													// -> [2]-[3]-[1]-[4]-[5]-...
		ps_exec_inst("ra", meta, stacka, stackb);	// -> [3]-[2]-[4]-[5]-...-[1]
													// -> [3]-[1]-[4]-[5]-...-[2]
		ps_exec_inst("sa", meta, stacka, stackb);	// -> [2]-[3]-[4]-[5]-...-[1]
													// -> [1]-[3]-[4]-[5]-...-[2]
		ps_exec_inst("rra", meta, stacka, stackb);	// -> [1]-[2]-[3]-[4]-[5]-...
													// -> [2]-[1]-[3]-[4]-[5]-...
	}

// ==================== case: [1]-[3]-[2]-[4]-[5]-... ====================
// ==================== case: [2]-[3]-[1]-[4]-[5]-... ====================
	if (biggest == 1)
	{
			ps_exec_inst("ra", meta, stacka, stackb);	// -> [3]-[2]-[4]-[5]-...-[1]
														// -> [3]-[1]-[4]-[5]-...-[2]
			ps_exec_inst("sa", meta, stacka, stackb);	// -> [2]-[3]-[4]-[5]-...-[1]
														// -> [1]-[3]-[4]-[5]-...-[2]
			ps_exec_inst("rra", meta, stacka, stackb);	// -> [1]-[2]-[3]-[4]-[5]-...
														// -> [2]-[1]-[3]-[4]-[5]-...
	}
// ==================== case: [2]-[1]-[3]-[4]-[5]-... ====================
	if (stacka->head->value > stacka->head->next->value)
		ps_exec_inst("sa", meta, stacka, stackb);
}

void			ps_sort_three(t_meta *meta, t_stk_h *stacka, t_stk_h *stackb)
{
	short			biggest;

// ==================== case: [3] ====================================
	if (stacka->len == 1)
		return ;

// ==================== case: [3]-[1]-[2]-[4]-[5]-... ====================
	if (stacka->len > 3)
		return (ps_sort_big(meta, stacka; stackb));

// ==================== case: [2]-[1] ================================
	if (stacka->len == 2 && stacka->head->value > stacka->head->next->value)
		return (ps_exec_inst("sa", meta, stacka, stackb));	// -> [1]-[2]

	biggest = ps_find_biggest(stacka);
// ==================== case: [3]-[1]-[2] ============================
// ==================== case: [3]-[2]-[1] ============================
	if (biggest == 0)
		ps_exec_inst("ra", meta, stacka, stackb);	// -> [1]-[2]-[3]
													// -> [2]-[1]-[3]

// ==================== case: [1]-[3]-[2] ============================
// ==================== case: [2]-[3]-[1] ============================
	else if (biggest == 1)
		ps_exec_inst("rra", meta, stacka, stackb);	// -> [2]-[1]-[3]
													// -> [1]-[2]-[3]

// ==================== case: [2]-[1]-[3] ============================
	if (stacka->head->value > stacka->head->next->value)
		ps_exec_inst("sa", meta, stacka, stackb);	// -> [1]-[2]-[3]
}