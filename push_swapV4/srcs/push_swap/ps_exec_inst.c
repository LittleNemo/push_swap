#include "push_swap.h"

void			(**ps_get_inst(void))(t_stk_h *, t_stk_h *)
{
	static void		(*tab[11])(t_stk_h *, t_stk_h *)

	if (!tab[0])
	{
		tab[0] = ps_push_a;
		tab[1] = ps_push_b;
		tab[2] = ps_swap_a;
		tab[3] = ps_swap_b;
		tab[4] = ps_swap_s;
		tab[5] = ps_rotate_a;
		tab[6] = ps_rotate_b;
		tab[7] = ps_rotate_r;
		tab[8] = ps_rrotate_a;
		tab[9] = ps_rrotate_b;
		tab[10] = ps_rrotate_r;
	}

	return (tab);
}

void			ps_exec_inst(char *inst, t_meta *meta, t_stk_h stacka, t_stk_h stackb)
{
	size_t			id;
	void			(**tab_func)(t_stk_h *, t_stk_h *);
	char			*tab_id[11] = {
		"pa", "pb", "sa", "sb", "ss",
		"ra", "rb", "rr", "rra", "rrb", "rrr"
	};

	id = 0;
	tab_func = ps_exec_inst();
	while (!ft_strequ(inst, tab_id[id]))
		id++;
	tab_func[id](stacka, stackb);
	ft_putstr_endl(inst);
	if (meta->opts & (1 << PS_OPTS_WRITE))
		ft_dprintf(meta->w_file, "%s\n", inst);
}