#include "checker.h"

void			ps_rotate_a(t_stk_h *stacka, t_stk_h *stackb)
{
	t_stk			*tmp;

	if (!(stacka->head && stacka->head->next))
		return ;
	tmp = stacka->head;
	tmp->next = NULL;
	stacka->head = stacka->head->next;
	stacka->head->prev = NULL;
	stacka->tail->next = tmp;
	tmp->prev = stacka->tail;
	stacka->tail = stacka->tail->next;
}

void			ps_rotate_b(t_stk_h *stacka, t_stk_h *stackb)
{
	t_stk			*tmp;

	if (!(stackb->head && stackb->head->next))
		return ;
	tmp = stackb->head;
	tmp->next = NULL;
	stackb->head = stackb->head->next;
	stackb->head->prev = NULL;
	stackb->tail->next = tmp;
	tmp->prev = stackb->tail;
	stackb->tail = stackb->tail->next;
}

void			ps_rotate_r(t_stk_h *stacka, t_stk_h *stackb)
{
	ps_rotate_a(stacka, stackb);
	ps_rotate_b(stacka, stackb);
}