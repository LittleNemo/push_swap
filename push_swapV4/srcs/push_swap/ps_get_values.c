#include "checker.h"

static void		ps_update_stack(t_meta *meta, t_stk_h *stacka, int value)
{
	t_stk			*fresh;

	if (!(fresh = (t_stk *)ft_memalloc(sizeof(*fresh))))
		ps_error(PS_ERROR_MALLOC, meta, stacka, NULL);
	fresh->value = value;
	stacka->len++;
	if (!stacka->head)
	{
		stacka->head = fresh;
		stacka->tail = fresh;
		return ;
	}
	fresh->prev = stacka->tail;
	stacka->tail->next = fresh;
	stacka->tail = fresh;
}

static char		*ps_get_values_args(char **av, t_meta *meta, t_stk_h *stacka)
{
	char			*str;
	char			*tmp;
	char			*fresh;

	if (!(str = ft_strjoin(*av, " ")))
		ps_error(PS_ERROR_MALLOC, meta, NULL, NULL);
	while (++av)
	{
		tmp = str;
		if (!(str = ft_strjoin(tmp, *av)) && ft_strdel(&tmp))
			ps_error(PS_ERROR_MALLOC, meta, NULL, NULL);
		tmp = str;
		if (!(str = ft_strjoin(tmp, " ")) && ft_strdel(&tmp))
			ps_error(PS_ERROR_MALLOC, meta, NULL, NULL);
	}
	if (!(fresh = ft_strctrim(str, "\t\n\v\f\r ")) && ft_strdel(&str))
		ps_error(PS_ERROR_MALLOC, meta, NULL, NULL);
	ft_strdel(&line);
	return (fresh);
}

static char		*ps_get_values_read(t_meta *meta, t_stk_h *stacka)
{
	char			*line;
	char			*fresh;

	if (get_next_line(meta->r_fd, &line) == -1)
	{
		ft_strdel(&line);
		ps_error(PS_ERROR_READ, meta, NULL, NULL);
	}
	if (!(fresh = ft_strctrim(line, "\t\n\v\f\r ")) && ft_strdel(&line))
		ps_error(PS_ERROR_MALLOC, meta, NULL, NULL);
	ft_strdel(&line);
	return (fresh);
}

void			ps_get_values(char **av, t_meta *meta, t_stk_h *stacka)
{
	char			*line;
	char			*str;
	long			value;

	if (meta->opts & (1 << PS_OPTS_READ))
		line = ps_get_values_read(meta, stacka);
	else
		line = ps_get_values_args(av, meta, stacka);
	str = line;
	if (!ft_strpbrk(str, "0123456789"))
		ps_error(PS_USAGE, meta, NULL, NULL);
	while ((value = ft_strtol(str, &str, 0)) || *str)
	{
		if (!ft_isspace(*end))
			ps_error(PS_ERROR_NUMERIC, meta, stacka, NULL);
		if (value < INT_MIN || INT_MAX < value)
			ps_error(PS_ERROR_INTEGER, meta, stacka, NULL);
		if (ps_search_dup(stacka, value))
			ps_error(PS_ERROR_DOUBLE, meta, stacka, NULL);
		ps_update_stack(meta, stacka, (int)value)
	}
	ft_strdel(&line);
}