#include "push_swap.h"

static void		ps_override_argv(char **av)
{
	int				i;

	i = -1;
	while (av[++i + 1])
		ft_strcpy(&av[i], &av[i + 1]);
	av[i] = NULL;
}

static void		ps_get_file_name(char **av, t_meta *meta, char mode)
{
	if (mode == 'r')
	{
		if (!(meta->r_file = ft_strdup(av)))
			ps_error(PS_ERROR_MALLOC, meta, NULL, NULL);
		if ((meta->r_fd = open(meta->r_file, O_RDONLY)) == -1)
			ps_error(PS_ERROR_OPEN, meta, NULL, NULL);
		meta->opts |= (1 << PS_OPTS_READ);
		ps_override_argv(av);
		return ;
	}
	if (!(meta->w_file = ft_strdup(av)))
		ps_error(PS_ERROR_MALLOC, meta, NULL, NULL);
	if ((meta->w_fd = open(meta->w_file, O_WDONLY | O_APPEND | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR)) == -1)
		ps_error(PS_ERROR_OPEN, meta, NULL, NULL);
	meta->opts |= (1 << PS_OPTS_WRITE);
	ps_override_argv(av);
}

void			ps_get_opts(char **av, t_meta *meta)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	while (av[++i] && av[i][0] == "-" && av[i][1] != "-" && ++meta->nb_opts)
	{
		j = 0;
		while (av[i][++j])
			if (ft_strequ("--verbose", av[i]) || av[i][j] == 'v')
				meta->opts |= (1 << PS_OPTS_VERBOSE);
			else if (ft_strequ("--read", av[i]) || av[i][j] == 'r')
				ps_get_file_name(av + i + 1, meta, 'r');
			else if (ft_strequ("--write", av[i]) || av[i][j] == 'w')
				ps_get_file_name(av + i + 1, meta, 'w');
			else
				ps_error(PS_USAGE, meta, NULL, NULL);
	}
	if (ft_strequ("--", av[i]))
		meta->nb_opts++;
}