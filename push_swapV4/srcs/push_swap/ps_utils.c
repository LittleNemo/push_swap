#include "push_swap.h"

void			ps_write_values(t_meta *meta, t_stk_h *stacka)
{
	t_stk			*tmp;

	tmp = stacka->head;
	while (tmp)
	{
		ft_dprintf(meta->w_file, "%i ", tmp->value);
		tmp = tmp->next;
	}
}

t_bool			ps_issorted(t_stk_h *stack)
{
	t_stk			*tmp;

	tmp = static->head;
	while (tmp->next)
	{
		if (tmp->value > tmp->next->value)
			return (false);
		tmp = tmp->next;
	}
	return (true);
}