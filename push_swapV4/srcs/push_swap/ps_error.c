#include "push_swap.h"

static void		ps_print_usage(t_meta *meta)
{
	size_t			len;

	len = ft_strlen("usage: ./push_swap ")
	ft_fprintf(stderr, "%s\n%*c%s\n%*c%s\n%*c%s\n\n", \
		"usage: ./push_swap [-r | --read <src_file>]", \
		len, ' ', "[-w | --write <dst_file>]", \
		len, ' ', "<values>");
	ft_fprintf(stderr, "\t%s\t\t%s\n\t%s\t\t%s\n\n", \
		"read", "Specifies the file that contains the instructions to check", \
		"write", "Specifies the output file where the result will be writen");
	ft_fprintf(stderr, "\t%s\n\t%s\n", \
		"The <values> must be numerical integers without duplicates values.", \
		"If a source file is specified, <values> are ignored.");
	if (meta)
		ps_del_meta(meta);
	exit(EXIT_FAILURE);
}

static char		**ps_get_errors(void)
{
	static char			*tab[] = {
		PS_MSG_MALLOC,
		PS_MSG_OPEN,
		PS_MSG_READ,
		PS_MSG_DOUBLE,
		PS_MSG_INTEGER,
		PS_MSG_NUMERIC,
		PS_MSG_FORMAT,
		NULL
	};

	return (tab);
}

void			ps_error(int code, t_meta *meta, t_stk_h stacka, t_stk_h stackb)
{
	char			**tab;

	if (code == PS_NO_ERROR)
		return ;
	tab = ps_get_errors();
	if (code == PS_ERROR_USAGE)
		ps_print_usage(meta);
	ft_fprintf(stderr, "%s\n", tab[code - 1]);
	if (stacka)
		ps_del_stack(stacka);
	if (stackb)
		ps_del_stack(stackb);
	if (meta)
		ps_del_meta(meta);
	exit(EXIT_FAILURE);
}