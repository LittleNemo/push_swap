#include "checker.h"

void			ck_swap_a(t_stk_h *stacka, t_stk_h *stackb)
{
	int				tmp;

	if (!(stacka->head && stacka->head->next))
		return ;
	tmp = stacka->head->value;
	stacka->head->value = stacka->head->next->value;
	stacka->head->next->value = tmp;
}

void			ck_swap_b(t_stk_h *stacka, t_stk_h *stackb)
{
	int				tmp;

	if (!(stackb->head && stackb->head->next))
		return ;
	tmp = stackb->head->value;
	stackb->head->value = stackb->head->next->value;
	stackb->head->next->value = tmp;
}

void			ck_swap_s(t_stk_h *stacka, t_stk_h *stackb)
{
	ck_swap_a(stacka, stackb);
	ck_swap_b(stacka, stackb);
}