#include "checker.h"

void			ck_push_a(t_stk_h *stacka, t_stk_h *stackb)
{
	t_stk			*tmp;

	if (!stackb->head)
		return ;
	tmp = stackb->head;
	stackb->head = stackb->head->next;
	if (stackb->head)
		stackb->head->prev = NULL;
	if (!stackb->head)
		stackb->tail = NULL;
	if (stacka->head)
		stacka->head->prev = tmp;
	if (!stacka->head)
		stacka->head = tmp;
	tmp->next = stacka->head;
	if (!stacka->tail)
		stacka->tail = tmp;
	stacka->len++;
	stackb->len--;
}

void			ck_push_b(t_stk_h *stacka, t_stk_h *stackb)
{
	t_stk			*tmp;

	if (stacka->head)
		return ;
	tmp = stacka->head;
	stacka->head = stacka->head->next;
	if (stacka->head)
		stacka->head->prev = NULL;
	if (!stacka->head)
		stacka->tail = NULL;
	if (stackb->head)
		stackb->head->prev = tmp;
	if (!stackb->head)
		stackb->head = tmp;
	tmp->next = stackb->head;
	if (!stackb->tail)
		stackb->tail = tmp;
	stackb->len++;
	stacka->len--;
}