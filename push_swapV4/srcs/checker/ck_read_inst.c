#include "checker.h"

void			(**ck_get_inst(void))(t_stk_h *, t_stk_h *)
{
	static void		(*tab[11])(t_stk_h *, t_stk_h *);

	if (!tab[0])
	{
		tab[0] = ck_push_a;
		tab[1] = ck_push_b;
		tab[2] = ck_swap_a;
		tab[3] = ck_swap_b;
		tab[4] = ck_swap_s;
		tab[5] = ck_rotate_a;
		tab[6] = ck_rotate_b;
		tab[7] = ck_rotate_r;
		tab[8] = ck_rrotate_a;
		tab[9] = ck_rrotate_b;
		tab[10] = ck_rrotate_r;
	}

	return (tab);
}

static short	ck_get_inst_id(char *line)
{
	short			id;
	char			*tab[12] = {
		"pa", "pb", "sa", "sb", "ss",
		"ra", "rb", "rr", "rra", "rrb", "rrr",
		NULL
	};

	id = -1;
	while (tab[++id])
		if (ft_strequ(line, tab[id]))
			return (id);
	return (-1);
}

static void		ck_parse_inst(t_meta *meta, t_stk_h *stacka, t_stk_h *stackb, char *line)
{
	short			id;
	size_t			len;
	void			(**tab)(t_stk_h *, t_stk_h *);

	len = ft_strlen(line);
	if (len > 3 || len < 2 || !(line[ft_strspn(line, "abpsr")]))
		ck_error(CK_ERROR_FORMAT, meta, stacka, stackb);
	tab = ck_get_inst();
	if ((id = ck_get_inst_id(line)) == -1)
		ck_error(CK_ERROR_FORMAT, meta, stacka, stackb);
	tab[id](stacka, stackb);
}

void			ck_read_inst(t_meta *meta, t_stk_h *stacka, t_stk_h *stackb)
{
	char			*line;
	int				gnl;

	while ((gnl = get_next_line(meta->r_fd, &line)) == 1)
	{
		ck_parse_inst(meta, stacka, stackb, line);
		if (meta->opts & (1 << CK_OPTS_VERBOSE))
			ck_print_stack(stacka, stackb, line);
		if (meta->opts & (1 << CK_OPTS_WRITE))
			ft_dprintf(meta->w_file, "%s\n", line);
		meta->nb_inst++;
		ft_strdel(&line);
	}
	ft_strdel(&line);
	ck_error(ft_abs(gnl * CK_ERROR_READ), meta, stacka, stackb);
}