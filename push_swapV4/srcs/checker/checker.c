#include "checker.h"

int				main(int ac, char **av)
{
	static t_meta	meta;
	static t_stk_h	stacka;
	static t_stk_h	stackb;
	static char		result[25];

	ck_get_opts(av, &meta);
	if (!(meta.opts & (1 << CK_OPTS_WRITE)))
		ck_error(!(ac - 1 - meta.nb_opts), &meta, NULL, NULL);
	ck_get_values(av + 1 + meta.nb_opts, &meta, &stacka);
	ck_read_inst(&meta, &stacka, &stackb);
	if (meta.opts & (1 << PS_OPTS_WRITE))
		ck_write_values(&meta, &stacka);
	ft_strcpy(result, "KO Instructions done ");
	if (ck_issorted(&stacka) && !stackb.len)
		ft_strcpy(result, "OK Instructions done ");
	ft_printf("%@%s[%zu]%@\n", !stackb.len, result, meta.nb_inst, EOC);
	if (meta.opts & (1 << CK_OPTS_WRITE))
		ft_dprintf(meta.w_fd, "#===== %s[%zu] =====", result, meta.nb_inst);
	ck_del_meta(&meta);
	ck_del_stack(&stacka);
	ck_del_stack(&stackb);
	return (0);
}