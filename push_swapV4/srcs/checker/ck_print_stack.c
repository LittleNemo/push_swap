#include "checker.h"

static void		ck_print_stackb(t_stk_h *stackb, int color)
{
	t_stk			*tmp;

	tmp = stackb->head;
	ft_printf("%@Stack B %@(%zu)%@:\t%@", CYAN, color, stackb->len, CYAN, EOC);
	while (tmp)
	{
		ft_printf(" %@%i%@", YELLOW, tmp->value, EOC);
		tmp = tmp->next;
	}
	ft_putchar('\n');
}

static void		ck_print_stacka(t_stk_h *stacka, int color)
{
	t_stk			*tmp;

	ft_printf("%@Stack A %@(%zu)%@:\t%@", CYAN, color, stacka->len, CYAN, EOC);
	if (!(tmp = stacka->head))
		return ;
	ft_printf(" %@%i%@", GREEN, tmp->value, EOC);
	tmp = tmp->next;
	while (tmp && tmp->next)
	{
		ft_printf(" %@%i%@", (tmp->value < tmp->next->value), tmp->value, EOC);
		tmp = tmp->next;
	}
	ft_putchar('\n');
}

void			ck_print_stack(t_stk_h *stacka, t_stk_h *stackb, char *line)
{
	ft_printf("\e[?1049h");
	ft_printf("%@Instruction:\t%@%s%@\n", LIGHT_BLUE, ORANGE, line, EOC);
	ck_print_stacka(stacka, !(stackb->len));
	ck_print_stackb(stackb, !(stackb->len));
	ft_printf("\033[80A");
	ft_printf("\e[?1049l");
}