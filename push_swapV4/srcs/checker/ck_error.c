#include "checker.h"

static void		ck_print_usage(t_meta *meta)
{
	size_t			len;

	len = ft_strlen("usage: ./checker ")
	ft_fprintf(stderr, "%s\n%*c%s\n%*c%s\n%*c%s\n\n", \
		"usage: ./checker [-v | --verbose]", \
		len, ' ', "[-r | --read <src_file>]", \
		len, ' ', "[-w | --write <dst_file>]", \
		len, ' ', "<values>");
	ft_fprintf(stderr, "\t%s\t\t%s\n\t%s\t\t%s\n\t%s\t\t%s\n\n", \
		"verbose", "For each step, print the state of both stacks", \
		"read", "Specifies the file that contains the instructions to check", \
		"write", "Specifies the output file where the result will be writen");
	ft_fprintf(stderr, "\t%s\n\t%s\n", \
		"The <values> must be numerical integers without duplicates values.", \
		"If a source file is specified, <values> are ignored.");
	if (meta)
		ck_del_meta(meta);
	exit(EXIT_FAILURE);
}

static char		**ck_get_errors(void)
{
	static char			*tab[] = {
		CK_MSG_MALLOC,
		CK_MSG_OPEN,
		CK_MSG_READ,
		CK_MSG_DOUBLE,
		CK_MSG_INTEGER,
		CK_MSG_NUMERIC,
		CK_MSG_FORMAT,
		NULL
	};

	return (tab);
}

void			ck_error(int code, t_meta *meta, t_stk_h *stacka, t_stk_h *stackb)
{
	char			**tab;

	if (code == CK_NO_ERROR)
		return ;
	tab = ck_get_errors();
	if (code == CK_ERROR_USAGE)
		ck_print_usage(meta);
	ft_fprintf(stderr, "%s\n", tab[code - 1]);
	if (stacka)
		ck_del_stack(stacka);
	if (stackb)
		ck_del_stack(stackb);
	if (meta)
		ck_del_meta(meta);
	exit(EXIT_FAILURE);
}