#include "checker.h"

void			ck_rrotate_a(t_stk_h *stacka, t_stk_h *stackb)
{
	t_stk			*tmp;

	if (!(stacka->tail && stacka->tail->prev))
		return ;
	tmp = stacka->tail;
	tmp->prev = NULL;
	stacka->tail = stacka->tail->prev;
	stacka->tail->next = NULL;
	stacka->head->prev = tmp;
	tmp->next = stacka->head;
	stacka->head = stacka->head->prev;
}

void			ck_rrotate_b(t_stk_h *stacka, t_stk_h *stackb)
{
	t_stk			*tmp;

	if (!(stackb->tail && stackb->tail->prev))
		return ;
	tmp = stackb->tail;
	tmp->prev = NULL;
	stackb->tail = stackb->tail->prev;
	stackb->tail->next = NULL;
	stackb->head->prev = tmp;
	tmp->next = stackb->head;
	stackb->head = stackb->head->prev;
}

void			ck_rrotate_r(t_stk_h *stacka, t_stk_h *stackb)
{
	ck_rrotate_a(stacka, stackb);
	ck_rrotate_b(stacka, stackb);
}