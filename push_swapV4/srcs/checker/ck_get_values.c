#include "checker.h"

static void		ck_update_stack(t_meta *meta, t_stk_h *stacka, int value)
{
	t_stk			*fresh;

	if (!(fresh = (t_stk *)ft_memalloc(sizeof(*fresh))))
		ck_error(CK_ERROR_MALLOC, meta, stacka, NULL);
	fresh->value = value;
	stacka->len++;
	if (!stacka->head)
	{
		stacka->head = fresh;
		stacka->tail = fresh;
		return ;
	}
	fresh->prev = stacka->tail;
	stacka->tail->next = fresh;
	stacka->tail = fresh;
}

static char		*ck_get_values_args(char **av, t_meta *meta, t_stk_h *stacka)
{
	char			*str;
	char			*tmp;
	char			*fresh;

	if (!(str = ft_strjoin(*av, " ")))
		ck_error(CK_ERROR_MALLOC, meta, NULL, NULL);
	while (++av)
	{
		tmp = str;
		if (!(str = ft_strjoin(tmp, *av)) && ft_strdel(&tmp))
			ck_error(CK_ERROR_MALLOC, meta, NULL, NULL);
		tmp = str;
		if (!(str = ft_strjoin(tmp, " ")) && ft_strdel(&tmp))
			ck_error(CK_ERROR_MALLOC, meta, NULL, NULL);
	}
	if (!(fresh = ft_strctrim(str, "\t\n\v\f\r ")) && ft_strdel(&str))
		ck_error(CK_ERROR_MALLOC, meta, NULL, NULL);
	ft_strdel(&line);
	return (fresh);
}

static char		*ck_get_values_read(t_meta *meta, t_stk_h *stacka)
{
	char			*line;
	char			*fresh;

	if (get_next_line(meta->r_fd, &line) == -1)
	{
		ft_strdel(&line);
		ck_error(CK_ERROR_READ, meta, NULL, NULL);
	}
	if (!(fresh = ft_strctrim(line, "\t\n\v\f\r ")) && ft_strdel(&line))
		ck_error(CK_ERROR_MALLOC, meta, NULL, NULL);
	ft_strdel(&line);
	return (fresh);
}

void			ck_get_values(char **av, t_meta *meta, t_stk_h *stacka)
{
	char			*line;
	char			*str;
	long			value;

	if (meta->opts & (1 << CK_OPTS_READ))
		line = ck_get_values_read(meta, stacka);
	else
		line = ck_get_values_args(av, meta, stacka);
	str = line;
	if (!ft_strpbrk(str, "0123456789"))
		ck_error(CK_USAGE, meta, NULL, NULL);
	while ((value = ft_strtol(str, &str, 0)) || *str)
	{
		if (!ft_isspace(*str))
			ck_error(CK_ERROR_NUMERIC, meta, stacka, NULL);
		if (value < INT_MIN || INT_MAX < value)
			ck_error(CK_ERROR_INTEGER, meta, stacka, NULL);
		if (ck_search_dup(stacka, value))
			ck_error(CK_ERROR_DOUBLE, meta, stacka, NULL);
		ck_update_stack(meta, stacka, (int)value);
	}
	ft_strdel(&line);
}