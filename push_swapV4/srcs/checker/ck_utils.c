#include "checker.h"

void			ck_write_values(t_meta *meta, t_stk_h *stacka)
{
	t_stk			*tmp;

	tmp = stacka->head;
	while (tmp)
	{
		ft_dprintf(meta->w_file, "%i ", tmp->value);
		tmp = tmp->next;
	}
}

t_bool			ck_issorted(t_stk_h *stack)
{
	t_stk			*tmp;

	tmp = stack->head;
	while (tmp->next)
	{
		if (tmp->value > tmp->next->value)
			return (false);
		tmp = tmp->next;
	}
	return (true);
}

t_bool			ck_search_dup(t_stk_h *stack, long value)
{
	t_stk			*tmp;

	tmp = stack->head;
	while (tmp)
	{
		if (tmp->value = (int)value)
			return (true);
		tmp = tmp->next;
	}
	return (false);
}