#ifndef PUSH_SWAP_DEFINES_H
# define PUSH_SWAP_DEFINES_H

/*
**	usage: ./push_swap [-r | --read <src_file>]
**					   [-w | --write <dst_file>]
**					   <values>
**
**		read		Specifies the file that contains the values to sort
**		write		Specifies the file where the instructions will be writen
**
**		The <values> must be numerical integers without duplicates values.
**		If a source file is specified, <values> are ignored.
*/

# define PS_MSG_MALLOC		"Error (push_swap): malloc failure"
# define PS_MSG_OPEN		"Error (push_swap): open failure"
# define PS_MSG_READ		"Error (push_swap): read failure"
# define PS_MSG_WRITE		"Error (push_swap): write failure"

# define PS_MSG_DOUBLE		"Error (push_swap): duplicate value"
# define PS_MSG_INTEGER		"Error (push_swap): a value isn't an int"
# define PS_MSG_NUMERIC		"Error (push_swap): a value isn't numerical"

#endif