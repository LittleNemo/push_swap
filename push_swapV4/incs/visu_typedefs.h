#ifndef VISU_TYPEDEFS_H
# define VISU_TYPEDEFS_H

typedef struct	s_stk {
	int				value;
	struct s_stk_h	*header;
	struct s_stk	*next;
	struct s_stk	*prev;
}				t_stk;

typedef struct	s_stk_h	{
	size_t			len;
	struct s_stk	*head;
	struct s_stk	*tail;
}				t_stk_h;

typedef enum	e_error {
	VS_NO_ERROR,
	VS_ERROR_USAGE,
	VS_ERROR_MALLOC,
	VS_ERROR_OPEN,
	VS_ERROR_READ,
	VS_ERROR_WRITE,
	VS_ERROR_DOUBLE,
	VS_ERROR_INTEGER,
	VS_ERROR_NUMERIC,
	VS_ERROR_FORMAT
}				t_error;

#endif
