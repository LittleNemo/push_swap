#ifndef PUSH_SWAP_TYPEDEFS_H
# define PUSH_SWAP_TYPEDEFS_H

typedef struct	s_meta {
	size_t			nb_inst;
	unsigned short	opts;
	unsigned short	nb_opts;
	int				*s_tab;
	int				l_tab;
	int				r_fd;
	int				w_fd;
	const char		*r_file;
	const char		*w_file;
}				t_meta;

typedef struct	s_stk {
	int				value;
	struct s_stk_h	*header;
	struct s_stk	*next;
	struct s_stk	*prev;
}				t_stk;

typedef struct	s_stk_h	{
	size_t			len;
	struct s_stk	*head;
	struct s_stk	*tail;
}				t_stk_h;

typedef enum	e_opts {
	PS_OPTS_VERBOSE,
	PS_OPTS_READ,
	PS_OPTS_WRITE
}				t_otps;

typedef enum	e_error {
	PS_NO_ERROR,
	PS_ERROR_USAGE,
	PS_ERROR_MALLOC,
	PS_ERROR_OPEN,
	PS_ERROR_READ,
	PS_ERROR_WRITE,
	PS_ERROR_DOUBLE,
	PS_ERROR_INTEGER,
	PS_ERROR_NUMERIC,
	PS_ERROR_FORMAT
}				t_error;

#endif
