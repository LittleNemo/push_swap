#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "libft_part_io.h"
# include "libft_part_memory.h"
# include "libft_part_string.h"
# include "libft_part_type.h"
# include "ft_printf.h"
# include "push_swap_defines.h"
# include "push_swap_typedefs.h"

void			ps_error(int code, t_meta *meta, t_stk_h stacka, t_stk_h stackb);

void			ps_get_opts(char **av, t_meta *meta);
void			ps_get_values(char **av, t_meta *meta, t_stk_h *stacka);
void			ps_get_s_tab(t_meta *meta, t_stk_h *stacka);

void			ps_exec_inst(char *inst, t_meta *meta, t_stk_h stacka, t_stk_h stackb);
void			ps_push_a(t_stk_h *stacka, t_stk_h *stackb);
void			ps_push_b(t_stk_h *stacka, t_stk_h *stackb);
void			ps_swap_a(t_stk_h *stacka, t_stk_h *stackb);
void			ps_swap_b(t_stk_h *stacka, t_stk_h *stackb);
void			ps_swap_s(t_stk_h *stacka, t_stk_h *stackb);
void			ps_rotate_a(t_stk_h *stacka, t_stk_h *stackb);
void			ps_rotate_b(t_stk_h *stacka, t_stk_h *stackb);
void			ps_rotate_r(t_stk_h *stacka, t_stk_h *stackb);
void			ps_rrotate_a(t_stk_h *stacka, t_stk_h *stackb);
void			ps_rrotate_b(t_stk_h *stacka, t_stk_h *stackb);
void			ps_rrotate_r(t_stk_h *stacka, t_stk_h *stackb);

void			ps_write_values(t_meta *meta, t_stk_h *stacka);
t_bool			ps_issorted(t_stk_h *stack);
void			ps_del_meta(t_meta *meta);
void			ps_del_stack(t_stk_h *stack);

#endif