#ifndef CHECKER_TYPEDEFS_H
# define CHECKER_TYPEDEFS_H

/*
**	typedef struct	s_inst {
**		char			*name;
**		void			(*func(void))(t_stk_h *, t_stk_h *);
**	}				t_inst;
*/

typedef struct	s_meta {
	size_t			nb_inst;
	unsigned short	opts;
	unsigned short	nb_opts;
	int				r_fd;
	int				w_fd;
	const char		*r_file;
	const char		*w_file;
}				t_meta;

typedef struct	s_stk {
	int				value;
	struct s_stk_h	*header;
	struct s_stk	*next;
	struct s_stk	*prev;
}				t_stk;

typedef struct	s_stk_h	{
	size_t			len;
	struct s_stk	*head;
	struct s_stk	*tail;
}				t_stk_h;

typedef enum	e_opts {
	CK_OPTS_VERBOSE,
	CK_OPTS_READ,
	CK_OPTS_WRITE
}				t_opts;

typedef enum	e_error {
	CK_NO_ERROR,
	CK_ERROR_USAGE,
	CK_ERROR_MALLOC,
	CK_ERROR_OPEN,
	CK_ERROR_READ,
	CK_ERROR_DOUBLE,
	CK_ERROR_INTEGER,
	CK_ERROR_NUMERIC,
	CK_ERROR_FORMAT
}				t_error;

#endif
