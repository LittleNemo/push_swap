#ifndef CHECKER_DEFINES_H
# define CHECKER_DEFINES_H

/*
**	usage: ./checker [-v | --verbose]
**					 [-r | --read <src_file>]
**					 [-w | --write <dst_file>]
**					 <values>
**
**		verbose		For each step, print the state of both stacks
**		read		Specifies the file that contains the instructions to check
**		write		Specifies the output file where the result will be writen
**
**		The <values> must be numerical integers without duplicates values.
**		If a source file is specified, <values> are ignored.
*/

# define CK_MSG_MALLOC		"Error (checker): malloc failure"
# define CK_MSG_OPEN		"Error (checker): open failure"
# define CK_MSG_READ		"Error (checker): read failure"

# define CK_MSG_DOUBLE		"Error (checker): duplicate value"
# define CK_MSG_INTEGER		"Error (checker): a value isn't an int"
# define CK_MSG_NUMERIC		"Error (checker): a value isn't numerical"
# define CK_MSG_FORMAT		"Error (checker): instruction not well formated"

#endif