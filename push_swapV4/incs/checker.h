#ifndef CHECKER_H
# define CHECKER_H

// # include "libft_part_io.h"
// # include "libft_part_memory.h"
// # include "libft_part_string.h"
// # include "libft_part_type.h"
// # include "ft_printf.h"
# include "checker_defines.h"
# include "checker_typedefs.h"

void			ck_error(int code, t_meta *meta, t_stk_h *stacka, t_stk_h *stackb);

void			ck_get_opts(char **av, t_meta *meta);
void			ck_get_values(char **av, t_meta *meta, t_stk_h *stacka);
void			ck_read_inst(t_meta *meta, t_stk_h *stacka, t_stk_h *stackb);

void			ck_push_a(t_stk_h *stacka, t_stk_h *stackb);
void			ck_push_b(t_stk_h *stacka, t_stk_h *stackb);
void			ck_swap_a(t_stk_h *stacka, t_stk_h *stackb);
void			ck_swap_b(t_stk_h *stacka, t_stk_h *stackb);
void			ck_swap_s(t_stk_h *stacka, t_stk_h *stackb);
void			ck_rotate_a(t_stk_h *stacka, t_stk_h *stackb);
void			ck_rotate_b(t_stk_h *stacka, t_stk_h *stackb);
void			ck_rotate_r(t_stk_h *stacka, t_stk_h *stackb);
void			ck_rrotate_a(t_stk_h *stacka, t_stk_h *stackb);
void			ck_rrotate_b(t_stk_h *stacka, t_stk_h *stackb);
void			ck_rrotate_r(t_stk_h *stacka, t_stk_h *stackb);

void			ck_print_stack(t_stk_h *stacka, t_stk_h *stackb, char *line);

void			ck_write_values(t_meta *meta, t_stk_h *stacka);
t_bool			ck_issorted(t_stk_h *stack);
t_bool			ck_search_dup(t_stk_h *stack, long value);
void			ck_del_meta(t_meta *meta);
void			ck_del_stack(t_stk_h *stack);

#endif