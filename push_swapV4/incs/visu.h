#ifndef VISU_H
# define VISU_H

# include "libft_part_io.h"
# include "libft_part_memory.h"
# include "libft_part_string.h"
# include "libft_part_type.h"
# include "ft_printf.h"
# include "visu_defines.h"
# include "visu_typedefs.h"

#endif