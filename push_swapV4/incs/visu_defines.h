#ifndef VISU_DEFINES_H
# define VISU_DEFINES_H

/*
**	usage: ./visu <values>
**
**		The values must be numerical integers without duplicates values.
*/

# define VS_MSG_MALLOC		"Error (visu): malloc failure"
# define VS_MSG_OPEN		"Error (visu): open failure"
# define VS_MSG_READ		"Error (visu): read failure"
# define VS_MSG_WRITE		"Error (visu): write failure"

# define VS_MSG_DOUBLE		"Error (visu): duplicate value"
# define VS_MSG_INTEGER		"Error (visu): a value isn't an int"
# define VS_MSG_NUMERIC		"Error (visu): a value isn't numerical"
# define VS_MSG_FORMAT		"Error (visu): instruction not well formated"

#endif