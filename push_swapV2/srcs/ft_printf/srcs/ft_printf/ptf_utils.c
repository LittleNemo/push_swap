/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/28 18:20:46 by lbrangie          #+#    #+#             */
/*   Updated: 2018/04/16 14:40:27 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ptf_isflag(char c)
{
	return (c == '#' || c == '-' || c == '+' || c == ' ' || c == 'h' || \
			c == 'l' || c == 'j' || c == 'z' || c == '.' || c == '*' || \
			c == '\'' || ft_isdigit(c));
}

int				ptf_wcharlen(wchar_t wchar)
{
	unsigned int	tmp;

	tmp = ft_numlen_ubase(wchar, 2);
	if (tmp < 8)
		return (1);
	else if (tmp < 12)
		return (2);
	else if (tmp < 17)
		return (3);
	else
		return (4);
}

int				ptf_wstrlen(wchar_t *wstr)
{
	int				len;
	unsigned int	tmp;

	len = 0;
	while (*wstr)
	{
		tmp = ft_numlen_ubase(*wstr++, 2);
		if (tmp < 8)
			len += 1;
		else if (tmp < 12)
			len += 2;
		else if (tmp < 17)
			len += 3;
		else
			len += 4;
	}
	return (len);
}

void			ptf_lltoa_base(t_flags *flags, t_conv *d, int base)
{
	char			*str_base;
	int				i;

	str_base = STR_BASE_LO;
	if (flags->type == 'X')
		str_base = STR_BASE_UP;
	i = 0;
	if ((flags->type == 'd' || flags->type == 'i' || flags->type == 'D') && \
		((intmax_t)d->n_tmp < 0))
	{
		d->n_sign[0] = '-';
		d->n_tmp = -d->n_tmp;
	}
	while (i < d->nlen)
	{
		d->n_str[d->nlen - i++ - 1] = str_base[ft_abs(d->n_tmp % base)];
		d->n_tmp /= base;
	}
	d->n_str[i] = '\0';
}
