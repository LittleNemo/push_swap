/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcslen.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/25 14:24:18 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/25 14:32:45 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t			ft_strcslen(const char *s, char *charset)
{
	size_t			size;

	size = 0;
	while (*s && !ft_ischarset(*s, charset))
	{
		size++;
		s++;
	}
	return (size);
}
