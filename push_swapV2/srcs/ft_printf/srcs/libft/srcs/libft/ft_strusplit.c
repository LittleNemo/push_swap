/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strusplit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/25 11:52:14 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/25 14:32:41 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_cntwrd(const char *s, char *charset)
{
	unsigned int	i;
	size_t			nb_wrd;
	unsigned char	lever;

	i = 0;
	nb_wrd = 0;
	lever = 1;
	while (s[++i])
	{
		if (ft_isascii(s[i]) && !ft_ischarset(s[i], charset))
		{
			if (lever)
				nb_wrd++;
			lever = 0;
		}
		if (ft_ischarset(s[i], charset))
			lever = 1;
	}
	return (nb_wrd);
}

char			**ft_strusplit(const char *s, char *charset)
{
	char			**tab;
	unsigned int	i;
	unsigned int	j;

	i = 0;
	j = 0;
	if (!s || !charset || \
		!(tab = (char**)malloc(sizeof(tab) * (ft_cntwrd(s, charset) + 2))))
		return (NULL);
	while (s[i])
	{
		while (ft_ischarset(s[i], charset))
			i++;
		if (s[i] && !ft_ischarset(s[i], charset))
		{
			if (!(tab[j++] = ft_strsub(s, i, ft_strcslen(&s[i], charset))))
				return (NULL);
			i += ft_strcslen(&s[i], charset);
		}
	}
	tab[j] = NULL;
	return (tab);
}
