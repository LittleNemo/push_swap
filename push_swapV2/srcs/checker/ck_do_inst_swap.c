/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_do_inst_swap.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/03 11:45:40 by lbrangie          #+#    #+#             */
/*   Updated: 2018/07/12 11:06:21 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void		ck_do_inst_swap_stack(t_stack **stack)
{
	t_stack			*tmp;

	if (ck_lstlen(*stack) >= 2)
	{
		tmp = (*stack)->nxt;
		(*stack)->nxt = tmp->nxt;
		tmp->nxt = *stack;
		*stack = tmp;
	}
}

int				ck_do_inst_swap(char stack, t_stack **st_a, t_stack **st_b)
{
	if (!ft_strchr("abs", stack))
		return (-1);
	if (stack == 'a' || stack == 's')
		ck_do_inst_swap_stack(st_a);
	if (stack == 'b' || stack == 's')
		ck_do_inst_swap_stack(st_b);
	return (0);
}
