/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_do_inst_push.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/02 14:39:04 by lbrangie          #+#    #+#             */
/*   Updated: 2018/07/12 10:53:59 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void		ck_do_inst_push_stack(t_stack **src, t_stack **dst)
{
	t_stack			*tmp;

	if (*src)
	{
		tmp = *src;
		*src = (*src)->nxt;
		tmp->nxt = *dst;
		*dst = tmp;
	}
}

int				ck_do_inst_push(char stack, t_stack **st_a, t_stack **st_b)
{
	if (!ft_strchr("ab", stack))
		return (-1);
	else if (stack == 'a')
		ck_do_inst_push_stack(st_b, st_a);
	else if (stack == 'b')
		ck_do_inst_push_stack(st_a, st_b);
	return (0);
}
