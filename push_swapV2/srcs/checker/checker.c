/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/28 14:14:19 by lbrangie          #+#    #+#             */
/*   Updated: 2018/07/06 14:30:24 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static int		ck_issorted(t_stack *stack)
{
	if (stack)
		while (stack->nxt)
		{
			if (stack->value > stack->nxt->value)
				return (0);
			stack = stack->nxt;
		}
	return (1);
}

int				main(int ac, char **av)
{
	t_stack			*st_a;
	t_stack			*st_b;
	short			opts;
	short			nb_opts;
	int				nb_inst;

	st_a = NULL;
	st_b = NULL;
	nb_opts = ck_get_opts(av + 1, &opts);
	ck_error(!(ac - 1 - nb_opts), 0);
	st_a = ck_get_values(av + nb_opts + 1);
	nb_inst = ck_read_inst(&st_a, &st_b, opts);
	if (ck_issorted(st_a) && !st_b)
	{
		ft_printf("%@OK%@\n", GREEN, EOC);
		if (opts & (1 << CK_OPTS_O))
			ft_printf("%@Done in |%d| instructions.%@\n", GREEN, nb_inst, EOC);
	}
	else
		ft_printf("%@KO%@\n", RED, EOC);
	ck_free_list(st_a);
	return (0);
}
