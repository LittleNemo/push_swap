/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ck_do_inst_rrotate.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/03 16:17:45 by lbrangie          #+#    #+#             */
/*   Updated: 2018/07/12 11:03:07 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void		ck_do_inst_rrotate_stack(t_stack **stack)
{
	t_stack			*tmp;

	if (ck_lstlen(*stack) >= 2)
	{
		tmp = *stack;
		while (tmp->nxt->nxt)
			tmp = tmp->nxt;
		tmp->nxt->nxt = *stack;
		*stack = tmp->nxt;
		tmp->nxt = NULL;
	}
}

int				ck_do_inst_rrotate(char stack, t_stack **st_a, t_stack **st_b)
{
	if (!ft_strchr("abr", stack))
		return (-1);
	if (stack == 'a' || stack == 'r')
		ck_do_inst_rrotate_stack(st_a);
	if (stack == 'b' || stack == 'r')
		ck_do_inst_rrotate_stack(st_b);
	return (0);
}
