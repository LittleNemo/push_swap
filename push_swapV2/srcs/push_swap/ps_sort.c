/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_sort.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 15:08:49 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/17 19:29:46 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_sort_infinity(t_stack **st_a, t_stack **st_b, \
				t_inst **inst, int size)
{
	double			med;
	int				origin;
	int				cnt;

	if (size == 1)
		return ;
	med = ps_find_med(*st_a, size);
	origin = 0;
	cnt = -1;
	while (++cnt < size)
		if ((*st_a)->value < med)
		{
			ps_do_inst("pb", st_a, st_b, inst);
			origin++;
		}
		else
			ps_do_inst("ra", st_a, st_b, inst);
	ps_do_n_rra(st_a, st_b, size - origin, inst);
	ps_do_n_pa(st_a, st_b, origin, inst);
	ps_sort_infinity(st_a, st_b, inst, origin);
	ps_do_n_ra(st_a, st_b, origin, inst);
	ps_sort_infinity(st_a, st_b, inst, size - origin);
	ps_do_n_rra(st_a, st_b, origin, inst);
}

static void		ps_sort_three(t_inst **inst, t_stack **st_a, t_stack **st_b)
{
	int				max_link;

	if (ps_lstlen(*st_a) == 1)
		return ;
	else if (ps_lstlen(*st_a) == 2 && (*st_a)->value > (*st_a)->nxt->value)
		ps_do_inst("sa", st_a, st_b, inst);
	else
	{
		max_link = ps_find_max_link(*st_a);
		if (max_link == 0)
			ps_do_inst("ra", st_a, st_b, inst);
		if (max_link == 1)
			ps_do_inst("rra", st_a, st_b, inst);
		if ((*st_a)->value > (*st_a)->nxt->value)
			ps_do_inst("sa", st_a, st_b, inst);
	}
}

void			ps_sort(t_stack **st_a, t_stack **st_b, t_inst **inst)
{
	if (ps_issorted(*st_a))
		return ;
	if (ps_lstlen(*st_a) <= 3)
		ps_sort_three(inst, st_a, st_b);
	else
		ps_sort_infinity(st_a, st_b, inst, (int)ps_lstlen(*st_a));
}
