/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_do_inst_rrotate.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 13:25:48 by lbrangie          #+#    #+#             */
/*   Updated: 2018/07/09 14:06:16 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_do_inst_rrotate_stack(t_stack **stack)
{
	t_stack			*tmp;

	if (ps_lstlen(*stack) >= 2)
	{
		tmp = *stack;
		while (tmp->nxt->nxt)
			tmp = tmp->nxt;
		tmp->nxt->nxt = *stack;
		*stack = tmp->nxt;
		tmp->nxt = NULL;
	}
}

int				ps_do_inst_rrotate(char stack, t_stack **st_a, t_stack **st_b)
{
	if (!ft_strchr("abr", stack))
		return (-1);
	if (stack == 'a' || stack == 'r')
		ps_do_inst_rrotate_stack(st_a);
	if (stack == 'b' || stack == 'r')
		ps_do_inst_rrotate_stack(st_b);
	return (0);
}
