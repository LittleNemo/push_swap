/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/06 14:44:46 by lbrangie          #+#    #+#             */
/*   Updated: 2018/07/06 14:49:21 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				ps_error(int error, char c)
{
	if (error == 0)
		return (0);
	else if (error == 1)
		ft_fprintf(stderr, "%s\n", PS_USAGE);
	else if (error == 2)
		ft_fprintf(stderr, "%s%c\n", PS_ERROR_ARGS, c);
	else if (error == 3)
		ft_fprintf(stderr, "%s\n", PS_ERROR_DOUBLE);
	else if (error == 4)
		ft_fprintf(stderr, "%s\n", PS_ERROR_MALLOC);
	else if (error == 5)
		ft_fprintf(stderr, "%s\n", PS_ERROR_NOT_INT);
	else if (error == 6)
		ft_fprintf(stderr, "%s\n", PS_ERROR_NOT_NUM);
	else
		ft_fprintf(stderr, "%s\n", PS_ERROR_UNKNOW);
	exit(EXIT_FAILURE);
}
