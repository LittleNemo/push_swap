/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_print_stacks.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 14:56:39 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/17 16:31:45 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_print_stack_b(t_stack *st_b, int color)
{
	t_stack			*tmp;
	size_t			size;

	tmp = st_b;
	size = ps_lstlen(st_b);
	ft_printf("%@Stack B %@(%zu)%@\t:%@", CYAN, color, size, CYAN, EOC);
	while (tmp)
	{
		ft_printf(" %@%ld%@", YELLOW, tmp->value, EOC);
		tmp = tmp->nxt;
	}
	ft_putchar('\n');
}

static void		ps_print_stack_a(t_stack *st_a, int color)
{
	t_stack			*tmp;
	t_stack			*tmp2;
	size_t			size;

	tmp = st_a;
	tmp2 = st_a;
	size = ps_lstlen(st_a);
	ft_printf("%@Stack A %@(%zu)%@\t:%@", CYAN, color, size, CYAN, EOC);
	if (tmp)
	{
		ft_printf(" %@%d%@", GREEN, tmp->value, EOC);
		tmp = tmp->nxt;
		while (tmp)
		{
			if (tmp->value < tmp2->value)
				ft_printf(" %@%ld%@", RED, tmp->value, EOC);
			else
				ft_printf(" %@%ld%@", GREEN, tmp->value, EOC);
			tmp = tmp->nxt;
			tmp2 = tmp2->nxt;
		}
	}
	ft_putchar('\n');
}

void			ps_print_stacks(t_stack *st_a, t_stack *st_b)
{
	int				color;

	color = GREEN;
	if (st_b)
		color = RED;
	ps_print_stack_a(st_a, color);
	ps_print_stack_b(st_b, color);
	ft_putchar('\n');
}

void			ps_print_inst(t_inst *inst, t_stack *st_a, \
				t_stack *st_b, short opts)
{
	while (inst)
	{
		ft_putstr_endl(inst->op);
		inst = inst->nxt;
	}
	if (opts & (1 << PS_OPTS_V))
		ps_print_stacks(st_a, st_b);
}
