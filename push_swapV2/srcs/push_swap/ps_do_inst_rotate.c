/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_do_inst_rotate.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 13:17:05 by lbrangie          #+#    #+#             */
/*   Updated: 2018/07/12 11:01:31 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_do_inst_rotate_stack(t_stack **stack)
{
	t_stack			*tmp;

	if (ps_lstlen(*stack) >= 2)
	{
		tmp = *stack;
		while (tmp->nxt)
			tmp = tmp->nxt;
		tmp->nxt = *stack;
		*stack = (*stack)->nxt;
		tmp->nxt->nxt = NULL;
	}
}

int				ps_do_inst_rotate(char stack, t_stack **st_a, t_stack **st_b)
{
	if (!ft_strchr("abr", stack))
		return (-1);
	if (stack == 'a' || stack == 'r')
		ps_do_inst_rotate_stack(st_a);
	if (stack == 'b' || stack == 'r')
		ps_do_inst_rotate_stack(st_b);
	return (0);
}
