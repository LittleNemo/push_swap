/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_utils2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/11 17:48:13 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/17 14:19:30 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			ps_free_inst(t_inst *inst)
{
	t_inst			*tmp;

	if (inst)
		free(inst->tail);
	while (inst)
	{
		tmp = inst;
		inst = inst->nxt;
		free(tmp);
	}
}

size_t			ps_nbinst(t_inst *inst)
{
	size_t			len;

	len = 0;
	while (inst)
	{
		len++;
		inst = inst->nxt;
	}
	return (len);
}

void			ps_inst_add(char *op, t_inst **inst)
{
	t_inst			*new;

	if (!(new = (t_inst *)malloc(sizeof(*new))))
		ps_error(4, 0);
	ft_bzero(new->op, 4);
	ft_strncpy(new->op, op, 3);
	new->nxt = NULL;
	if (*inst == NULL)
	{
		*inst = new;
		new->tail = (t_inst **)malloc(sizeof(t_inst *));
	}
	else
	{
		(*(*inst)->tail)->nxt = new;
		new->tail = (*inst)->tail;
	}
	*new->tail = new;
}
