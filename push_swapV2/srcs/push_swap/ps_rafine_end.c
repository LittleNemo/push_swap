/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_rafine_end.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/19 14:35:09 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/19 15:19:05 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		ps_rafine_next(t_inst *here)
{
	t_inst			*forward;

	forward = here;
	while (forward)
	{
		if (!(ft_strequ(here->op, forward->op)))
			return (0);
		forward = forward->nxt;
	}
	return (1);
}

t_inst			*ps_rafine_end(t_inst *inst, int nb_val)
{
	t_inst			*here;
	t_inst			*start;
	int				nb;

	here = inst;
	start = inst;
	while (!ps_rafine_next(here))
		here = here->nxt;
ft_printf("here->op: %s\n", here->op);
	nb = ps_nbinst(here);
	inst = here;
	inst->nxt = NULL;
	ps_free_inst(here);
	nb_val -= nb;
	inst = start;
	while (nb_val--)
	{
ft_printf("loooooooooool\n");
		ps_inst_add("ra", &inst);
	}
	inst = start;
	return (inst);
}
