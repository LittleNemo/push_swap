/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_do_n_inst.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/17 16:34:37 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/17 16:55:59 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			ps_do_n_rra(t_stack **st_a, t_stack **st_b, int n, t_inst **inst)
{
	while (n--)
		if (ps_do_inst("rra", st_a, st_b, inst) == -1)
			return (-1);
	return (0);
}

int			ps_do_n_ra(t_stack **st_a, t_stack **st_b, int n, t_inst **inst)
{
	while (n--)
		if (ps_do_inst("ra", st_a, st_b, inst) == -1)
			return (-1);
	return (0);
}

int			ps_do_n_pa(t_stack **st_a, t_stack **st_b, int n, t_inst **inst)
{
	while (n--)
		if (ps_do_inst("pa", st_a, st_b, inst) == -1)
			return (-1);
	return (0);
}
