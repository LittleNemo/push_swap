/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_get_opts.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/06 14:41:01 by lbrangie          #+#    #+#             */
/*   Updated: 2018/07/06 14:44:29 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_get_opts_update(char c, short *opts)
{
	if (c == 'v')
		*opts |= (1 << PS_OPTS_V);
	else if (c == 'o')
		*opts |= (1 << PS_OPTS_O);
	else
		ps_error(2, c);
}

short			ps_get_opts(char **av, short *opts)
{
	int				i;
	int				j;
	short			nb_opts;

	*opts = 0;
	nb_opts = 0;
	i = -1;
	while (av[++i] && av[i][0] == '-' && ft_str_isalpha(av[i] + 1))
	{
		j = 0;
		while (av[i][++j])
			ps_get_opts_update(av[i][j], opts);
		nb_opts++;
	}
	return (nb_opts);
}
