/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_do_inst.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 12:45:07 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/17 18:08:39 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				ps_do_n_rra(t_stack **st_a, t_stack **st_b, \
				int n, t_inst **inst)
{
	while (n--)
		if (ps_do_inst("rra", st_a, st_b, inst) == -1)
			return (-1);
	return (0);
}

int				ps_do_n_ra(t_stack **st_a, t_stack **st_b, int n, t_inst **inst)
{
	while (n--)
		if (ps_do_inst("ra", st_a, st_b, inst) == -1)
			return (-1);
	return (0);
}

int				ps_do_n_pa(t_stack **st_a, t_stack **st_b, int n, t_inst **inst)
{
	while (n--)
		if (ps_do_inst("pa", st_a, st_b, inst) == -1)
			return (-1);
	return (0);
}

int				ps_do_inst(char *op, t_stack **st_a, \
				t_stack **st_b, t_inst **inst)
{
	int				ret;
	size_t			len;

	ret = -1;
	len = ft_strlen(op);
	if (len == 2)
	{
		if (op[0] == 'p')
			ret = ps_do_inst_push(op[1], st_a, st_b);
		else if (op[0] == 's')
			ret = ps_do_inst_swap(op[1], st_a, st_b);
		else if (op[0] == 'r')
			ret = ps_do_inst_rotate(op[1], st_a, st_b);
	}
	else if (len == 3 && ft_strnequ(op, "rr", 2))
		ret = ps_do_inst_rrotate(op[2], st_a, st_b);
	if (!ret && inst)
		ps_inst_add(op, inst);
	return (ret);
}
