/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_do_inst_swap.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 12:08:24 by lbrangie          #+#    #+#             */
/*   Updated: 2018/07/09 14:05:51 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_do_inst_swap_stack(t_stack **stack)
{
	t_stack			*tmp;

	if (ps_lstlen(*stack) >= 2)
	{
		tmp = (*stack)->nxt;
		(*stack)->nxt = tmp->nxt;
		tmp->nxt = *stack;
		*stack = tmp;
	}
}

int				ps_do_inst_swap(char stack, t_stack **st_a, t_stack **st_b)
{
	if (!ft_strchr("abs", stack))
		return (-1);
	if (stack == 'a' || stack == 's')
		ps_do_inst_swap_stack(st_a);
	if (stack == 'b' || stack == 's')
		ps_do_inst_swap_stack(st_b);
	return (0);
}
