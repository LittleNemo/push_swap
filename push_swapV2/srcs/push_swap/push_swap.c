/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/06 14:08:06 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/20 15:15:50 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				main(int ac, char **av)
{
	t_stack			*st_a;
	t_stack			*st_b;
	t_inst			*inst;
	short			opts;
	short			nb_opts;

	st_a = NULL;
	st_b = NULL;
	inst = NULL;
	nb_opts = ps_get_opts(av + 1, &opts);
	ps_error(!(ac - 1 - nb_opts), 0);
	st_a = ps_get_values(av + nb_opts + 1);
	ps_sort(&st_a, &st_b, &inst);
	ps_rafine_inst(inst);
	ps_print_inst(inst, st_a, st_b, opts);
	if ((opts & (1 << PS_OPTS_O)) && ps_issorted(st_a) && !st_b)
		ft_printf("%@Done in %d instrutions.%@\n", GREEN, ps_nbinst(inst), EOC);
	ps_free_list(st_a);
	ps_free_inst(inst);
	return (0);
}
