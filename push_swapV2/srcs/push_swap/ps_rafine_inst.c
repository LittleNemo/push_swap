/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_rafine_inst.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/17 14:24:24 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/17 14:58:58 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static t_inst	*ps_rafine_inst_rotate(t_inst *inst)
{
	if (inst->nxt == NULL)
		return (NULL);
	if (!ft_strcmp(inst->op, "ra") && !ft_strcmp(inst->nxt->op, "rra"))
		return (inst->nxt->nxt);
	if (!ft_strcmp(inst->op, "rra") && !ft_strcmp(inst->nxt->op, "ra"))
		return (inst->nxt->nxt);
	return (inst);
}

static t_inst	*ps_rafine_inst_push(t_inst *inst)
{
	if (inst->nxt == NULL)
		return (NULL);
	if (!ft_strcmp(inst->op, "pa") && !ft_strcmp(inst->nxt->op, "pb"))
		return (inst->nxt->nxt);
	if (!ft_strcmp(inst->op, "pb") && !ft_strcmp(inst->nxt->op, "pa"))
		return (inst->nxt->nxt);
	return (inst);
}

t_inst			*ps_rafine_inst(t_inst *inst)
{
	t_inst			*ret;

	ret = inst;
	while (ret)
	{
		ret = inst;
		if (inst->op[0] == 'p')
			ret = ps_rafine_inst_push(inst);
		if (inst->op[0] == 'r')
			ret = ps_rafine_inst_rotate(inst);
		if (ret == inst)
		{
			if ((ret = ps_rafine_inst(inst->nxt)))
			{
				free(inst->nxt->nxt);
				free(inst->nxt);
				inst->nxt = ret;
			}
		}
		else
			return (ret);
	}
	return (NULL);
}
