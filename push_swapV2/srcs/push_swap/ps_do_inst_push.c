/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_do_inst_push.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 11:45:44 by lbrangie          #+#    #+#             */
/*   Updated: 2018/07/09 14:03:52 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ps_do_inst_push_stack(t_stack **src, t_stack **dst)
{
	t_stack			*tmp;

	if (*src)
	{
		tmp = *src;
		*src = (*src)->nxt;
		tmp->nxt = *dst;
		*dst = tmp;
	}
}

int				ps_do_inst_push(char stack, t_stack **st_a, t_stack **st_b)
{
	if (!ft_strchr("ab", stack))
		return (-1);
	else if (stack == 'a')
		ps_do_inst_push_stack(st_b, st_a);
	else if (stack == 'b')
		ps_do_inst_push_stack(st_a, st_b);
	return (0);
}
