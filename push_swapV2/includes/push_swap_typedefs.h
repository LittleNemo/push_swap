/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_typedefs.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/28 12:58:34 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/13 18:59:17 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_TYPEDEFS_H
# define PUSH_SWAP_TYPEDEFS_H

typedef enum	e_opts
{
	PS_OPTS_V,
	PS_OPTS_O
}				t_opts;

typedef struct	s_stack
{
	long			value;
	struct s_stack	*nxt;
}				t_stack;

typedef struct	s_inst
{
	char			op[4];
	struct s_inst	**tail;
	struct s_inst	*nxt;
}				t_inst;

#endif
