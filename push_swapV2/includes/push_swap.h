/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/28 11:57:49 by lbrangie          #+#    #+#             */
/*   Updated: 2018/09/19 15:19:23 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <limits.h>
# include <stdlib.h>
# include <stdio.h>
# include "../srcs/ft_printf/includes/ft_printf.h"
# include "push_swap_defines.h"
# include "push_swap_typedefs.h"

int				ps_error(int error, char c);

short			ps_get_opts(char **av, short *opts);
t_stack			*ps_get_values(char **av);

int				ps_do_inst(char *op, t_stack **st_a, \
				t_stack **st_b, t_inst **inst);
int				ps_do_n_rra(t_stack **st_a, t_stack **st_b, \
				int n, t_inst **inst);
int				ps_do_n_ra(t_stack **st_a, t_stack **st_b, \
				int n, t_inst **inst);
int				ps_do_n_pa(t_stack **st_a, t_stack **st_b, \
				int n, t_inst **inst);
int				ps_do_inst_push(char inst, t_stack **st_a, t_stack **st_b);
int				ps_do_inst_swap(char inst, t_stack **st_a, t_stack **st_b);
int				ps_do_inst_rotate(char inst, t_stack **st_a, t_stack **st_b);
int				ps_do_inst_rrotate(char inst, t_stack **st_a, t_stack **st_b);

double			ps_find_med(t_stack *stack, int size);
void			ps_sort(t_stack **st_a, t_stack **st_b, t_inst **inst);
t_inst			*ps_rafine_inst(t_inst *inst);

void			ps_print_stacks(t_stack *st_a, t_stack *st_b);
void			ps_print_inst(t_inst *inst, t_stack *st_a, \
				t_stack *st_b, short opts);

int				ps_issorted(t_stack *stack);
void			ps_free_list(t_stack *list);
int				ps_find_max_link(t_stack *stack);
size_t			ps_lstlen(t_stack *stack);
t_stack			*ps_lst_findlast(t_stack *list);

void			ps_free_inst(t_inst *inst);
size_t			ps_nbinst(t_inst *inst);
void			ps_inst_add(char *op, t_inst **inst);

#endif
