# PUSH_SWAP

Deuxieme projet d'algorithmie de 42.
Ce projet nous demande de coder un algorithme de tri d'entier avec quelques restrictions.
Le sujet detaille du projet est disponible [ici](https://cdn.intra.42.fr/pdf/pdf/650/push_swap.fr.pdf).

## Les regles

Ce projet a pour but de nous forcer a creer un algorithme de tri, donc.
Nous disposons de **deux piles**:
- dans la pile A, il y a une liste de nombres entiers dans le desordre
- dans la pile B, il n'y a rien (au debut en tous cas)

Pour trier ces entiers, nous ne pouvons pas faire n'importe quoi, nous n'avons que certaines operations de disponible:
1. **pa** (push a): prend le premier élément au sommet de b et le met sur a. Ne fait rien si b est vide.
2. **pb** (push b): prend le premier élément au sommet de a et le met sur b. Ne fait rien si a est vide.
3. **sa** (swap a): intervertit les 2 premiers éléments au sommet de la pile a. Ne fait rien s’il n’y en a qu’un ou aucun.
4. **sb** (swap b): intervertit les 2 premiers éléments au sommet de la pile b. Ne fait rien s’il n’y en a qu’un ou aucun.
5. **ss** : sa et sb en même temps.
6. **ra** (rotate a): décale d’une position vers le haut tous les élements de la pile a. Le premier élément devient le dernier.
7. **rb** (rotate b): décale d’une position vers le haut tous les élements de la pile b. Le premier élément devient le dernier.
8. **rr** : ra et rb en même temps.
9. **rra** (reverse rotate a): décale d’une position vers le bas tous les élements de la pile a. Le dernier élément devient le premier.
10. **rrb** (reverse rotate b): décale d’une position vers le bas tous les élements de la pile b. Le dernier élément devient le premier.
11. **rrr** : rra et rrb en même temps.

## Le programme push_swap

Le programme push_swap prend en parametre une liste d'entier et doit les trier en faisant le moins d'instructions
possibles. Les entier doivent etre ecrits uniquement en chiffres. "un" et "one" ne sont donc pas des valeurs valides.
En cas d'erreur, "Error" est affiche.
Le programme push_swap se lance de la facon suivante: `./push_swap value1 ...`

## Le programme checker

Le programme checker prend en parametre une liste d'entier et lit ensuite des instructions sur l'entree standard. Une
fois toutes les instructions lues, le programme les execute sur la pile d'entier. Si, apres exectuion, la pile A est
triee dans l'ordre croissant et que la pile B est vide, le checker affiche "OK" sur la sortie standard. Sinon, il
affiche "KO".
Comme pour le programme push_swap, si une erreur se produit, lors de l'aquisition des entiers par exemple, "Error" est
afficher.
Le programme checker se lance de la facon suivante: `./checker value1 ...`
